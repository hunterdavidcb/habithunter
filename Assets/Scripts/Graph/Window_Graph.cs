﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Window_Graph : MonoBehaviour
{
	[SerializeField]
	private Sprite dotSprite;
	private RectTransform graphContainer;
	private RectTransform labelTemplateX;
	private RectTransform labelTemplateY;
	private RectTransform dashTemplateX;
	private RectTransform dashTemplateY;
	private GameObject tooltipGameObject;

	private List<GameObject> graphObjects = new List<GameObject>();
	//public List<DateTime> dates = new List<DateTime>();

	private void Awake()
	{
		graphContainer = transform.Find("graphContainer").GetComponent<RectTransform>();

		labelTemplateX = graphContainer.Find("labelTemplateX").GetComponent<RectTransform>();
		labelTemplateY = graphContainer.Find("labelTemplateY").GetComponent<RectTransform>();

		dashTemplateX = graphContainer.Find("dashTemplateY").GetComponent<RectTransform>();
		dashTemplateY = graphContainer.Find("dashTemplateX").GetComponent<RectTransform>();

		tooltipGameObject = graphContainer.Find("tooltip").gameObject;

		//List<int> valueList = new List<int>() { 5, 98, 56, 76, 23, 49, 67, 29, 40, 15, 3 };
		
	}

	public void SetData(List<float> success, List<DateTime> dates)
	{
		//for (int i = 0; i < success.Count; i++)
		//{
		//	Debug.Log(success[i]);
		//}
		IGraphVisual igv = new LineGraphVisual(graphContainer, dotSprite, Color.green, new Color(1, 1, 1, 0.5f));
		Func<int, string> xLabel = delegate (int i) { return dates[i].ToShortDateString(); };
		ShowGraph(success, igv, -1, xLabel);
	}

	public void ShowTooltip(string text, Vector2 pos)
	{
		tooltipGameObject.SetActive(true);
		Text t = tooltipGameObject.transform.Find("text").GetComponent<Text>();
		t.text = text;
		Vector2 backgroundSize = new Vector2( t.preferredWidth + 8f, t.preferredHeight + 8f);
		tooltipGameObject.transform.Find("background").GetComponent<RectTransform>().sizeDelta = backgroundSize;
		tooltipGameObject.GetComponent<RectTransform>().anchoredPosition = pos;
		tooltipGameObject.transform.SetAsLastSibling();
	}

	public void HideTooltip(string text, Vector2 pos)
	{
		tooltipGameObject.SetActive(false);
	}

	private void ShowGraph(List<int> valueList, IGraphVisual igv, int maxValuesShown = -1, Func<int,string> getAxisLabelX = null, Func<float, string> getAxisLabelY = null)
	{
		float graphHeight = graphContainer.sizeDelta.y;
		float graphWidth = graphContainer.sizeDelta.x;
		if (maxValuesShown <= 0)
		{
			maxValuesShown = valueList.Count;
		}

		float yMax = valueList[0];
		float yMin = valueList[0];

		//making the y axis dynamic
		for (int i = Mathf.Max(valueList.Count - maxValuesShown,0); i < valueList.Count; i++)
		{
			int value = valueList[i];
			if (value > yMax)
			{
				yMax = value;
			}

			if (value < yMin)
			{
				yMin = value;
			}
		}

		yMin = 0f;
		float yDiff = yMax - yMin;
		if (yDiff <= 0f)
		{
			yDiff = 5f;
		}

		yMax = yMax + (yDiff * .2f);
		//yMin = yMin - (yDiff * .2f);

		//forces graph to start at 0 on the y axis
		//yMin = 0f;

		float xSize = graphWidth / (maxValuesShown + 1);
		

		//I do not understand this code
		if (getAxisLabelX == null)
		{
			getAxisLabelX = delegate (int _i) { return _i.ToString(); };
		}

		if (getAxisLabelY == null)
		{
			getAxisLabelY = delegate (float _i) { return Mathf.RoundToInt(_i).ToString(); };
		}

		foreach (var go in graphObjects)
		{
			Destroy(go);
		}
		graphObjects.Clear();

		int xIndex = 0;
		for (int i = Mathf.Max(valueList.Count - maxValuesShown, 0); i < valueList.Count; i++)
		{
			//Debug.Log(valueList[i]);
			float xPos = xSize + xIndex * xSize;
			float yPos = ((valueList[i] - yMin) / (yMax-yMin)) * graphHeight;

			string tooltip = getAxisLabelY(valueList[i]);
			graphObjects.AddRange(igv.AddGraphVisual(new Vector2(xPos, yPos), xSize, tooltip));

			RectTransform labelX = Instantiate(labelTemplateX);
			graphObjects.Add(labelX.gameObject);
			labelX.SetParent(graphContainer,false);
			labelX.gameObject.SetActive(true);
			labelX.anchoredPosition = new Vector2(xPos, -20f);
			labelX.GetComponent<Text>().text = getAxisLabelX(i);

			RectTransform dashX = Instantiate(dashTemplateX);
			graphObjects.Add(dashX.gameObject);
			dashX.SetParent(graphContainer, false);
			dashX.gameObject.SetActive(true);
			dashX.anchoredPosition = new Vector2(xPos, 0f);

			xIndex++;
		}

		int sepCount = 10;
		for (int i = 0; i <= sepCount; i++)
		{
			RectTransform labelY = Instantiate(labelTemplateY);
			graphObjects.Add(labelY.gameObject);
			labelY.SetParent(graphContainer,false);
			labelY.gameObject.SetActive(true);
			float normalizedValue = i * 1f / sepCount;
			labelY.anchoredPosition = new Vector2(-7f, normalizedValue * graphHeight);
			labelY.GetComponent<Text>().text = getAxisLabelY(yMin + (normalizedValue * (yMax - yMin)));

			RectTransform dashY = Instantiate(dashTemplateY);
			graphObjects.Add(dashY.gameObject);
			dashY.SetParent(graphContainer, false);
			dashY.gameObject.SetActive(true);
			normalizedValue = i * 1f / sepCount;
			dashY.anchoredPosition = new Vector2(0f, normalizedValue * graphHeight);
		}
	}

	private void ShowGraph(List<float> valueList, IGraphVisual igv, int maxValuesShown = -1, Func<int, string> getAxisLabelX = null, Func<float, string> getAxisLabelY = null)
	{
		if (valueList.Count == 0)
		{
			return;
		}

		//Debug.Log(maxValuesShown);
		float graphHeight = graphContainer.sizeDelta.y;
		float graphWidth = graphContainer.sizeDelta.x;

		if (maxValuesShown <= 0)
		{
			maxValuesShown = valueList.Count;
		}

		//Debug.Log(maxValuesShown);

		float yMax = 101f; //= valueList[0];
		float yMin = 0f; //= valueList[0];

		//making the y axis dynamic
		//for (int i = Mathf.Max(valueList.Count - maxValuesShown, 0); i < valueList.Count; i++)
		//{
		//	float value = valueList[i];
		//	if (value > yMax)
		//	{
		//		yMax = value;
		//	}

		//	if (value < yMin)
		//	{
		//		yMin = value;
		//	}
		//}

		//yMin = 0f;
		float yDiff = yMax - yMin;
		if (yDiff <= 0f)
		{
			yDiff = 5f;
		}

		//yMax = yMax + (yDiff * .2f);
		//yMin = yMin - (yDiff * .2f);

		//forces graph to start at 0 on the y axis
		//yMin = 0f;

		float xSize = graphWidth / (maxValuesShown + 1);


		//I do not understand this code
		if (getAxisLabelX == null)
		{
			getAxisLabelX = delegate (int _i) { return _i.ToString(); };
		}

		if (getAxisLabelY == null)
		{
			getAxisLabelY = delegate (float _i) { return Mathf.RoundToInt(_i).ToString(); };
		}

		foreach (var go in graphObjects)
		{
			Destroy(go);
		}
		graphObjects.Clear();

		int xIndex = 0;
		for (int i = Mathf.Max(valueList.Count - maxValuesShown, 0); i < valueList.Count; i++)
		{
			float xPos = xSize + xIndex * xSize;
			float yPos = ((valueList[i] - yMin) / (yMax - yMin)) * graphHeight;

			string tooltip = getAxisLabelY(valueList[i]);
			graphObjects.AddRange(igv.AddGraphVisual(new Vector2(xPos, yPos), xSize, tooltip));

			RectTransform labelX = Instantiate(labelTemplateX);
			graphObjects.Add(labelX.gameObject);
			labelX.SetParent(graphContainer, false);
			labelX.gameObject.SetActive(true);
			labelX.anchoredPosition = new Vector2(xPos, -20f);
			labelX.GetComponent<Text>().text = getAxisLabelX(i);

			RectTransform dashX = Instantiate(dashTemplateX);
			graphObjects.Add(dashX.gameObject);
			dashX.SetParent(graphContainer, false);
			dashX.gameObject.SetActive(true);
			dashX.anchoredPosition = new Vector2(xPos, 0f);

			xIndex++;
		}

		int sepCount = 10;
		for (int i = 0; i <= sepCount; i++)
		{
			RectTransform labelY = Instantiate(labelTemplateY);
			graphObjects.Add(labelY.gameObject);
			labelY.SetParent(graphContainer, false);
			labelY.gameObject.SetActive(true);
			float normalizedValue = i * 1f / sepCount;
			labelY.anchoredPosition = new Vector2(-7f, normalizedValue * graphHeight);
			labelY.GetComponent<Text>().text = getAxisLabelY(normalizedValue * 100);//yMin + (normalizedValue * (yMax - yMin)));

			RectTransform dashY = Instantiate(dashTemplateY);
			graphObjects.Add(dashY.gameObject);
			dashY.SetParent(graphContainer, false);
			dashY.gameObject.SetActive(true);
			normalizedValue = i * 1f / sepCount;
			dashY.anchoredPosition = new Vector2(0f, normalizedValue * graphHeight);
		}
	}

	private interface IGraphVisual
	{
		List<GameObject> AddGraphVisual(Vector2 graphPos, float graphWidth, string tooltip);
	}

	private class BarChartVisual : IGraphVisual
	{
		private RectTransform graphContainer;
		private Color barColor;
		private float barWidthMultiplier;

		public BarChartVisual(RectTransform gc, Color bc, float barWM)
		{
			graphContainer = gc;
			barColor = bc;
			barWidthMultiplier = barWM;
		}

		public List<GameObject> AddGraphVisual(Vector2 graphPos, float graphWidth, string tooltip)
		{
			GameObject go = CreateBar(graphPos, graphWidth);
			Tooltip tp = go.AddComponent<Tooltip>();
			tp.message = tooltip;
			tp.PointerEntered += graphContainer.GetComponentInParent<Window_Graph>().ShowTooltip;
			tp.PointerExited += graphContainer.GetComponentInParent<Window_Graph>().HideTooltip;
			return new List<GameObject>() { go };
		}

		private GameObject CreateBar(Vector2 pos, float barWidth)
		{
			GameObject gameObject = new GameObject("bar", typeof(Image));
			gameObject.transform.SetParent(graphContainer, false);
			RectTransform rect = gameObject.GetComponent<RectTransform>();
			gameObject.GetComponent<Image>().color = barColor;
			rect.anchoredPosition = new Vector2(pos.x, 0f);
			rect.sizeDelta = new Vector2(barWidth * barWidthMultiplier, pos.y);
			rect.anchorMin = new Vector2(0, 0);
			rect.anchorMax = new Vector2(0, 0);
			rect.pivot = new Vector2(0.5f, 0f);
			return gameObject;
		}
	}

	private class LineGraphVisual : IGraphVisual
	{
		private RectTransform graphContainer;
		private Sprite dotSprite;
		private Color lineColor;
		private Color dotColor;

		private GameObject previousDot;

		public LineGraphVisual(RectTransform gc, Sprite dot, Color lc, Color dc)
		{
			graphContainer = gc;
			dotSprite = dot;
			lineColor = lc;
			dotColor = dc;
			previousDot = null;
		}

		public List<GameObject> AddGraphVisual(Vector2 graphPos, float graphWidth, string tooltip)
		{
			GameObject dotGameObject = CreateDot(graphPos);
			Tooltip tp = dotGameObject.AddComponent<Tooltip>();
			tp.message = tooltip;
			tp.PointerEntered += graphContainer.GetComponentInParent<Window_Graph>().ShowTooltip;
			tp.PointerExited += graphContainer.GetComponentInParent<Window_Graph>().HideTooltip;
			//graphObjects.Add(dotGameObject);
			GameObject connection = null;
			if (previousDot != null)
			{
				connection = CreateDotConnection(previousDot.GetComponent<RectTransform>().anchoredPosition,
					dotGameObject.GetComponent<RectTransform>().anchoredPosition);

			}

			previousDot = dotGameObject;
			return new List<GameObject>() { dotGameObject, connection };
		}

		

		private GameObject CreateDot(Vector2 anchoredPos)
		{
			GameObject gameObject = new GameObject("dot", typeof(Image));
			gameObject.transform.SetParent(graphContainer, false);
			gameObject.GetComponent<Image>().sprite = dotSprite;
			gameObject.GetComponent<Image>().color = dotColor;
			RectTransform rect = gameObject.GetComponent<RectTransform>();
			rect.anchoredPosition = anchoredPos;
			rect.sizeDelta = new Vector2(11, 11);
			rect.anchorMin = new Vector2(0, 0);
			rect.anchorMax = new Vector2(0, 0);
			return gameObject;
		}

		private GameObject CreateDotConnection(Vector2 posA, Vector2 posB)
		{
			GameObject gameObject = new GameObject("dotConnection", typeof(Image));
			gameObject.transform.SetParent(graphContainer, false);
			gameObject.GetComponent<Image>().raycastTarget = false;
			gameObject.GetComponent<Image>().color = lineColor;
			RectTransform rect = gameObject.GetComponent<RectTransform>();
			Vector2 dir = (posB - posA).normalized;
			float distance = Vector2.Distance(posA, posB);
			rect.anchoredPosition = posA + dir * distance * 0.5f;
			rect.sizeDelta = new Vector2(distance, 3);
			rect.anchorMin = new Vector2(0, 0);
			rect.anchorMax = new Vector2(0, 0);

			rect.localEulerAngles = new Vector3(0, 0, Mathf.Rad2Deg * Mathf.Atan2(dir.y, dir.x));

			return gameObject;
		}
	}
}
