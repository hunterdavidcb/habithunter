﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AllHabitsObject : MonoBehaviour
{
	public Text habitName;
	public Text habitPattern;
	public Text habitSuccess;
	public Text habitStreak;

	public Button removeHabitButton;

}
