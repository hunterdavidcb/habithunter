﻿using Ical.Net;
using Ical.Net.DataTypes;
using Ical.Net.Interfaces.DataTypes;
using Ical.Net.Serialization.iCalendar.Serializers.DataTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class HabitViewerPanel : MonoBehaviour
{
	[SerializeField]
	private Sprite dotSprite;

	public Sprite ring;
	public Sprite circle;
	public GameObject line;

	public Button nextHabitButton;
	public Button previousHabitButton;
	public Button nameEditButton;
	public Button ruleEditButton;
	public Button viewStreaksButton;
	public Button viewGraphButton;

	public Button nameEditAcceptButton;
	public Button ruleEditAcceptButton;

	public Button nameEditCancelButton;
	public Button ruleEditCancelButton;

	public GameObject nameEditPanel;
	public InputField habitNameInput;
	public GameObject ruleEditPanel;
	

	public Habit habit;

	public Text habitName;
	public Text habitRule;

	public Dropdown repititionDropdown;
	public Dropdown ordinalDropdown;
	public Dropdown months;
	public Dropdown daysOfWeek;
	public Dropdown ending;
	public Dropdown month;
	public Slider dayOfMonth;
	public InputField interval, count, length;
	public Dropdown unitName;
	public GameObject lengthEntry;
	public Toggle hasLength;
	public Toggle onThe;
	public Toggle onDay;
	public GameObject endDateEntryHolder, timeEntryHolder;
	public GameObject weeklyHolder;
	public Text errorText;

	public Button colorButton;
	public Button acceptColorButton;
	public ColorPickerTriangle colorPicker;
	public GameObject colorBlocker;

	public Window_Graph windowGraph;

	public StreakViewer streakViewer;

	public Transform horizontalScrollView;
	RectTransform horizontal;
	private RectTransform labelTemplateX;
	private RectTransform labelTemplateY;
	private RectTransform dashTemplateX;
	private RectTransform dashTemplateY;
	private GameObject tooltipGameObject;

	InputField endYearInput, endMonthInput, endDayInput;
	InputField timeHourInput, timeMinuteInput;
	Toggle monday, tuesday, wednesday, thursday, friday, saturday, sunday;

	public delegate void ViewHabitHandler(Habit habit);
	public event ViewHabitHandler ViewHabitStreaks;

	Dictionary<DateTime, List<GameObject>> habitStreakObjects = new Dictionary<DateTime, List<GameObject>>();

	private List<GameObject> graphObjects = new List<GameObject>();

	private void OnEnable()
	{
		colorButton.onClick.AddListener(OnColorPickerButtonCliked);
		acceptColorButton.onClick.AddListener(OnColorPicked);
	}

	// Start is called before the first frame update
	void Awake()
    {
		horizontal = horizontalScrollView.GetComponent<RectTransform>();


		labelTemplateX = horizontalScrollView.Find("labelTemplateX").GetComponent<RectTransform>();
		labelTemplateY = horizontalScrollView.Find("labelTemplateY").GetComponent<RectTransform>();

		dashTemplateX = horizontalScrollView.Find("dashTemplateY").GetComponent<RectTransform>();
		dashTemplateY = horizontalScrollView.Find("dashTemplateX").GetComponent<RectTransform>();

		tooltipGameObject = horizontalScrollView.Find("tooltip").gameObject;

		nameEditButton.onClick.AddListener(OnNameEditClicked);
		ruleEditButton.onClick.AddListener(OnRuleEditClicked);

		colorButton.onClick.AddListener(OnColorPickerButtonCliked);
		acceptColorButton.onClick.AddListener(OnColorPicked);

		nameEditAcceptButton.onClick.AddListener(OnFinishNameEdit);
		nameEditCancelButton.onClick.AddListener(OnCancelNameEdit);

		ruleEditAcceptButton.onClick.AddListener(OnFinishRuleEdit);
		ruleEditCancelButton.onClick.AddListener(OnCancelRuleEdit);

		viewStreaksButton.onClick.AddListener(OnViewStreaks);

		viewGraphButton.onClick.AddListener(OnViewGraph);

		colorPicker.gameObject.SetActive(false);
		colorBlocker.SetActive(false);
		viewGraphButton.gameObject.SetActive(false);

		errorText.gameObject.SetActive(false);
		//panel.SetActive(false);

		endYearInput = endDateEntryHolder.transform.GetChild(0).GetComponent<InputField>();
		endMonthInput = endDateEntryHolder.transform.GetChild(1).GetComponent<InputField>();
		endDayInput = endDateEntryHolder.transform.GetChild(2).GetComponent<InputField>();

		timeHourInput = timeEntryHolder.transform.GetChild(0).GetComponent<InputField>();
		timeMinuteInput = timeEntryHolder.transform.GetChild(1).GetComponent<InputField>();

		monday = weeklyHolder.transform.GetChild(0).GetComponent<Toggle>();
		tuesday = weeklyHolder.transform.GetChild(1).GetComponent<Toggle>();
		wednesday = weeklyHolder.transform.GetChild(2).GetComponent<Toggle>();
		thursday = weeklyHolder.transform.GetChild(3).GetComponent<Toggle>();
		friday = weeklyHolder.transform.GetChild(4).GetComponent<Toggle>();
		saturday = weeklyHolder.transform.GetChild(5).GetComponent<Toggle>();
		sunday = weeklyHolder.transform.GetChild(6).GetComponent<Toggle>();


		List<string> options = new List<string>();
		onDay.onValueChanged.AddListener(delegate { ToggleChanged(); });
		onThe.onValueChanged.AddListener(delegate { ToggleChanged(); });

		endYearInput.onValueChanged.AddListener(delegate {
			OnInputValueChanged(4, endYearInput.text, endYearInput, endMonthInput);
		});

		endMonthInput.onValueChanged.AddListener(delegate {
			OnInputValueChanged(2, endMonthInput.text, endMonthInput, endDayInput);
		});

		endDayInput.onValueChanged.AddListener(delegate {
			OnInputValueChanged(2, endDayInput.text, endDayInput, null);
		});

		//Debug.Log(ending.onValueChanged.GetPersistentTarget(0).name);

		foreach (var item in Enum.GetValues(typeof(EndType)))
		{
			options.Add(item.ToString());
		}

		ending.ClearOptions();
		ending.AddOptions(options);

		months.ClearOptions();
		options = new List<string>();
		foreach (var mon in Enum.GetValues(typeof(Months)))
		{
			options.Add(mon.ToString());
		}
		months.AddOptions(options);

		month.ClearOptions();
		options = new List<string>();
		foreach (var mon in Enum.GetValues(typeof(Months)))
		{
			options.Add(mon.ToString());
		}
		month.AddOptions(options);

		daysOfWeek.ClearOptions();
		options = new List<string>();
		foreach (var day in Enum.GetValues(typeof(DayOfWeek)))
		{
			options.Add(day.ToString());
		}
		daysOfWeek.AddOptions(options);

		ordinalDropdown.ClearOptions();
		options = new List<string>();
		foreach (var item in Enum.GetValues(typeof(Ordinal)))
		{
			options.Add(item.ToString());
		}

		ordinalDropdown.AddOptions(options);
		ordinalDropdown.onValueChanged.AddListener(OnOrdinalChanged);


		unitName.ClearOptions();
		options = new List<string>();
		foreach (var item in Enum.GetValues(typeof(RepititionUnitType)))
		{
			options.Add(item.ToString());
		}

		unitName.AddOptions(options);
		unitName.onValueChanged.AddListener(delegate { OnRepititionUnitChanged(); });

		ending.onValueChanged.AddListener(EndingChanged);

		monday.onValueChanged.AddListener(delegate { CheckWeekValidity(); });
		tuesday.onValueChanged.AddListener(delegate { CheckWeekValidity(); });
		wednesday.onValueChanged.AddListener(delegate { CheckWeekValidity(); });
		thursday.onValueChanged.AddListener(delegate { CheckWeekValidity(); });
		friday.onValueChanged.AddListener(delegate { CheckWeekValidity(); });
		saturday.onValueChanged.AddListener(delegate { CheckWeekValidity(); });
		sunday.onValueChanged.AddListener(delegate { CheckWeekValidity(); });

		errorText.gameObject.SetActive(false);

		hasLength.onValueChanged.AddListener(HasLengthToggleChanged);
		lengthEntry.SetActive(false);

		//InitializeRepititionType();
	}

	protected void OnInputValueChanged(int length, string text, InputField current, InputField next)
	{
		Debug.Log("received");
		if (text.Length == length && next != null)
		{
			current.DeactivateInputField();
			next.ActivateInputField();
		}
		else if (text.Length == length)
		{
			current.DeactivateInputField();
		}
	}

	private void HasLengthToggleChanged(bool hl)
	{
		lengthEntry.SetActive(hl);
	}

	protected void OnViewStreaks()
	{
		//if (ViewHabitStreaks != null)
		//{
		//	ViewHabitStreaks(habit);
		//}
		//List<HabitStreak> habitStreaks = new List<HabitStreak>();
		//foreach (var item in habit.HabitStreaks)
		//{
		//	habitStreaks.Add(item.Value);
		//}
		//windowGraph.gameObject.SetActive(false);

		//streakViewer.gameObject.SetActive(true);

		//streakViewer.SetStreaks(habit.HabitStreaks, habit.HabitColor);
		foreach (var go in graphObjects)
		{
			Destroy(go);
		}
		graphObjects.Clear();

		SetStreaks(habit.HabitStreaks, habit.HabitColor);

		viewStreaksButton.gameObject.SetActive(false);
		viewGraphButton.gameObject.SetActive(true);
	}

	protected void OnViewGraph()
	{
		//streakViewer.gameObject.SetActive(false);

		//windowGraph.gameObject.SetActive(true);
		//SetGraph();
		ClearObjects();

		SetScroll();

		viewStreaksButton.gameObject.SetActive(true);
		viewGraphButton.gameObject.SetActive(false);
	}

	protected void OnNameEditClicked()
	{
		nameEditPanel.SetActive(true);
	}

	protected void OnFinishNameEdit()
	{
		habit.SetName(habitNameInput.text);
		//serialize it
		MainMenu.instance.UpdateHabit(habit);

		SetHabit(habit);
		//close the panel
		nameEditPanel.SetActive(false);
	}

	protected void OnCancelNameEdit()
	{
		//close the panel
		nameEditPanel.SetActive(false);
		//reset the input field
		((Text)habitNameInput.placeholder).text = "Enter text";
		habitNameInput.text = "";
	}

	protected void OnRuleEditClicked()
	{
		ruleEditPanel.SetActive(true);
	}

	protected void OnRepititionUnitChanged()
	{

	}

	protected void OnFinishRuleEdit()
	{
		//validate here
		if (!IsValidName())
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "Please enter a name";
			return;
		}

		if ((EndType)ending.value == EndType.ON && !IsValidEndDate())
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "End date is not valid";
			return;
		}
		else if ((EndType)ending.value == EndType.AFTER && !IsValidCount())
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "Count must be positive";
			return;
		}

		int i = Int32.Parse(interval.text);
		if (i <= 0)
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "Interval must be positive abd greater than zero";
			return;
		}

		errorText.gameObject.SetActive(false);

		var pattern = new RecurrencePattern();
		//{
		//	Frequency = FrequencyType.Weekly,
		//	ByDay = new List<IWeekDay> { new WeekDay(DayOfWeek.Friday) },
		//	ByMonth = new List<int> { 1}

		//};

		switch ((RepititionType)repititionDropdown.value)
		{
			case RepititionType.SECONDLY:
				pattern.Frequency = FrequencyType.Secondly;
				break;
			case RepititionType.MINUTELY:
				pattern.Frequency = FrequencyType.Minutely;
				break;
			case RepititionType.HOURLY:
				pattern.Frequency = FrequencyType.Hourly;
				break;
			case RepititionType.DAILY:
				pattern.Frequency = FrequencyType.Daily;
				break;
			case RepititionType.WEEKLY:
				pattern.Frequency = FrequencyType.Weekly;

				pattern.ByDay = new List<IWeekDay>();
				if (monday.isOn)
				{
					pattern.ByDay.Add(new WeekDay(DayOfWeek.Monday));
				}

				if (tuesday.isOn)
				{
					pattern.ByDay.Add(new WeekDay(DayOfWeek.Tuesday));
				}

				if (wednesday.isOn)
				{
					pattern.ByDay.Add(new WeekDay(DayOfWeek.Wednesday));
				}

				if (thursday.isOn)
				{
					pattern.ByDay.Add(new WeekDay(DayOfWeek.Thursday));
				}

				if (friday.isOn)
				{
					pattern.ByDay.Add(new WeekDay(DayOfWeek.Friday));
				}

				if (saturday.isOn)
				{
					pattern.ByDay.Add(new WeekDay(DayOfWeek.Saturday));
				}

				if (sunday.isOn)
				{
					pattern.ByDay.Add(new WeekDay(DayOfWeek.Sunday));
				}


				break;
			case RepititionType.MONTHLY:
				pattern.Frequency = FrequencyType.Monthly;
				if (onDay.isOn)
				{
					pattern.ByMonthDay = new List<int>() { (int)dayOfMonth.value };
				}
				else
				{
					pattern.BySetPosition = new List<int>() { (int)(Enum.Parse(typeof(Ordinal),
						ordinalDropdown.options[ordinalDropdown.value].text)) };

					pattern.ByDay = new List<IWeekDay>() { new WeekDay((DayOfWeek)daysOfWeek.value) };
				}
				break;
			case RepititionType.YEARLY:
				pattern.Frequency = FrequencyType.Yearly;
				if (onDay.isOn)
				{
					pattern.ByMonth = new List<int>() { months.value + 1 };
					pattern.ByMonthDay = new List<int>() { (int)dayOfMonth.value };
				}
				else
				{
					pattern.ByMonth = new List<int>() { month.value + 1 };

					pattern.BySetPosition = new List<int>() { (int)(Enum.Parse(typeof(Ordinal),
						ordinalDropdown.options[ordinalDropdown.value].text)) };

					pattern.ByDay = new List<IWeekDay>() { new WeekDay((DayOfWeek)daysOfWeek.value) };
				}
				break;
			default:
				break;
		}

		switch ((RepititionType)repititionDropdown.value)
		{
			case RepititionType.SECONDLY:
			case RepititionType.MINUTELY:
			case RepititionType.HOURLY:
				//we don't care about the time entry
				break;
			case RepititionType.DAILY:
			case RepititionType.WEEKLY:
			case RepititionType.MONTHLY:
			case RepititionType.YEARLY:
				//we need to set the time entry
				pattern.ByHour = new List<int>() { Int32.Parse(timeHourInput.text) };
				pattern.ByMinute = new List<int>() { Int32.Parse(timeMinuteInput.text) };
				break;
			default:
				break;
		}

		pattern.Interval = Int32.Parse(interval.text);

		if ((EndType)ending.value == EndType.AFTER)
		{
			pattern.Count = Int32.Parse(count.text);
		}
		else if ((EndType)ending.value == EndType.ON)
		{
			//we should do sanity checks here
			pattern.Until = new DateTime(Int32.Parse(endYearInput.text), Int32.Parse(endMonthInput.text), Int32.Parse(endDayInput.text));
		}
		//pattern.u



		RecurrencePatternSerializer serializer = new RecurrencePatternSerializer();

		string pat = serializer.SerializeToString(pattern);

		RepUnit ru = new RepUnit();
		if (hasLength.isOn)
		{
			if (!string.IsNullOrEmpty(length.text))
			{
				
				ru.numberOfReps = Int32.Parse(length.text);
				ru.unitType = (RepititionUnitType)unitName.value;
			}
		}
	}

	protected void OnCancelRuleEdit()
	{
		//reset the values to display correctly
		InitializeRepititionType();

		//close the panel
		ruleEditPanel.SetActive(false);
	}

	public void SetHabit(Habit h)
	{
		habit = h;

		habitName.text = h.HabitName;
		habitRule.text = new RecurrencePattern(h.RepititionRules[0]).ConvertToSentence();

		colorButton.image.color = new Color(habit.HabitColorR, habit.HabitColorG, habit.HabitColorB);

		//SetGraph();

		SetScroll();

		//TestScroll();

		InitializeRepititionType();
	}

	public void ClearObjects()
	{
		foreach (var item in habitStreakObjects)
		{
			foreach (var ob in item.Value)
			{
				Destroy(ob);
			}
			item.Value.Clear();
		}

		habitStreakObjects.Clear();
	}

	public void SetStreaks(Dictionary<DateTime, HabitStreak> streaks, Color habitColor)
	{
		ClearObjects();
		Debug.Log("called");

		int maxX = 6;
		float width = 1000f;
		float height = 600f;
		foreach (var item in streaks)
		{
			//we can show it on one line
			if (item.Value.Instances.Count < maxX)
			{
				RectTransform previousPos = null;
				for (int i = 0; i < item.Value.Instances.Count; i++)
				{
					Debug.Log("inside");
					GameObject go = new GameObject("ring", typeof(Image));
					go.GetComponent<Image>().sprite = ring;
					go.GetComponent<Image>().color = habitColor;
					go.transform.SetParent(horizontal, false);
					RectTransform rect = go.GetComponent<RectTransform>();
					rect.anchoredPosition = new Vector2((width / (maxX + 5)) + ((width / maxX) * i), height / 2);
					rect.anchorMin = new Vector2(0, 0);
					rect.anchorMax = new Vector2(0, 0);

					if (!habitStreakObjects.ContainsKey(item.Key))
					{
						habitStreakObjects.Add(item.Key, new List<GameObject>() { go });
					}
					else
					{
						habitStreakObjects[item.Key].Add(go);
					}

					go = new GameObject("circle", typeof(Image));
					go.GetComponent<Image>().sprite = circle;
					go.GetComponent<Image>().color = new Color(habitColor.r, habitColor.g, habitColor.b, 0.5f);
					go.transform.SetParent(horizontal, false);
					rect = go.GetComponent<RectTransform>();
					rect.anchoredPosition = new Vector2((width / (maxX + 5)) + ((width / maxX) * i), height / 2);
					rect.anchorMin = new Vector2(0, 0);
					rect.anchorMax = new Vector2(0, 0);

					if (previousPos != null)
					{
						//we need to make a connection to the previous
						GameObject l = CreateDotConnection(rect.anchoredPosition, previousPos.anchoredPosition, habitColor);
						if (habitStreakObjects[item.Key] == null)
						{
							habitStreakObjects.Add(item.Key, new List<GameObject>() { l });
						}
						else
						{
							habitStreakObjects[item.Key].Add(l);
						}
					}

					if (habitStreakObjects[item.Key] == null)
					{
						habitStreakObjects.Add(item.Key, new List<GameObject>() { go });
					}
					else
					{
						habitStreakObjects[item.Key].Add(go);
					}

					RectTransform labelX = Instantiate(labelTemplateX);

					if (habitStreakObjects[item.Key] == null)
					{
						habitStreakObjects.Add(item.Key, new List<GameObject>() { labelX.gameObject });
					}
					else
					{
						habitStreakObjects[item.Key].Add(labelX.gameObject);
					}

					labelX.SetParent(horizontal, false);
					labelX.gameObject.SetActive(true);
					labelX.anchoredPosition = rect.anchoredPosition;
					//test this
					labelX.GetComponent<Text>().text = item.Value.Instances.ToList()[i].ToShortDateString();

					previousPos = rect;
				}
			}
			else
			{
				//we need to split it over several lines
				// get the number of lines needed
				int lineCount = Mathf.CeilToInt((float)item.Value.Instances.Count / maxX);
				Debug.Log(lineCount);
				for (int i = 0; i < lineCount; i++)
				{
					Debug.Log((i + 1) * maxX);
					int perLine = i < lineCount - 1 ? maxX : item.Value.Instances.Count - (i * maxX);//((i + 1) * maxX) < item.Value.Instances.Count?
																									 //	(item.Value.Instances.Count - (i + 1) * maxX) % maxX 
																									 //	: item.Value.Instances.Count % (i * maxX);
					Debug.Log(perLine);
					// item.Value.Instances.Count 
					RectTransform previousPos = null;
					for (int x = 0; x < perLine; x++)
					{
						//Debug.Log("inside");
						GameObject go = new GameObject("ring", typeof(Image));
						go.GetComponent<Image>().sprite = ring;
						go.GetComponent<Image>().color = habitColor;
						go.transform.SetParent(horizontal, false);
						RectTransform rect = go.GetComponent<RectTransform>();
						rect.anchoredPosition = new Vector2((width / (maxX + 5)) + ((width / maxX) * x),
							height - 50f - (height / lineCount) * i);
						rect.anchorMin = new Vector2(0, 0);
						rect.anchorMax = new Vector2(0, 0);

						if (!habitStreakObjects.ContainsKey(item.Key))
						{
							habitStreakObjects.Add(item.Key, new List<GameObject>() { go });
						}
						else
						{
							habitStreakObjects[item.Key].Add(go);
						}

						go = new GameObject("circle", typeof(Image));
						go.GetComponent<Image>().sprite = circle;
						go.GetComponent<Image>().color = new Color(habitColor.r, habitColor.g, habitColor.b, 0.5f);
						go.transform.SetParent(horizontal, false);
						rect = go.GetComponent<RectTransform>();
						rect.anchoredPosition = new Vector2((width / (maxX + 5)) + ((width / maxX) * x),
							height - 50f - (height / lineCount) * i);
						rect.anchorMin = new Vector2(0, 0);
						rect.anchorMax = new Vector2(0, 0);

						if (previousPos != null)
						{
							//we need to make a connection to the previous
							GameObject l = CreateDotConnection(rect.anchoredPosition, previousPos.anchoredPosition, habitColor);
							if (habitStreakObjects[item.Key] == null)
							{
								habitStreakObjects.Add(item.Key, new List<GameObject>() { l });
							}
							else
							{
								habitStreakObjects[item.Key].Add(l);
							}
						}

						if (habitStreakObjects[item.Key] == null)
						{
							habitStreakObjects.Add(item.Key, new List<GameObject>() { go });
						}
						else
						{
							habitStreakObjects[item.Key].Add(go);
						}

						RectTransform labelX = Instantiate(labelTemplateX);

						if (habitStreakObjects[item.Key] == null)
						{
							habitStreakObjects.Add(item.Key, new List<GameObject>() { labelX.gameObject });
						}
						else
						{
							habitStreakObjects[item.Key].Add(labelX.gameObject);
						}

						labelX.SetParent(horizontal, false);
						labelX.gameObject.SetActive(true);
						labelX.anchoredPosition = rect.anchoredPosition;
						//test this
						labelX.GetComponent<Text>().text = item.Value.Instances.ToList()[i * maxX + x].ToShortDateString();

						previousPos = rect;
					}
				}

			}
		}
	}

	private GameObject CreateDotConnection(Vector2 posA, Vector2 posB, Color habitColor)
	{
		GameObject gameObject = Instantiate(line); //new GameObject("dotConnection", typeof(Image));
		gameObject.transform.SetParent(horizontal, false);
		gameObject.GetComponent<Image>().raycastTarget = false;
		gameObject.GetComponent<Image>().color = new Color(habitColor.r, habitColor.g, habitColor.b, 0.75f);
		RectTransform rect = gameObject.GetComponent<RectTransform>();
		Vector2 dir = (posB - posA).normalized;
		float distance = Vector2.Distance(posA, posB);
		rect.anchoredPosition = posA + dir * distance * 0.5f;
		rect.sizeDelta = new Vector2(distance - 85, 20);
		rect.anchorMin = new Vector2(0, 0);
		rect.anchorMax = new Vector2(0, 0);

		rect.localEulerAngles = new Vector3(0, 0, Mathf.Rad2Deg * Mathf.Atan2(dir.y, dir.x));

		return gameObject;
	}

	private void TestScroll()
	{
		List<float> values = new List<float>();
		//get the dates
		List<DateTime> times = new List<DateTime>();
		int count = UnityEngine.Random.Range(30, 100);
		for (int i = 0; i < count; i++)
		{
			times.Add(DateTime.Today.AddDays(-count + i));
		}
		
		//sort them
		//times.Sort();

		for (int i = 0; i < times.Count; i++)
		{
			//Debug.Log(habit.SuccessPercentAtDate(times[i]));
			values.Add(UnityEngine.Random.Range(0f,100f));
		}

		Debug.Log(times.Count);

		SpawnGraphItems(values, times);
	}

	private void SetScroll()
	{
		List<float> values = new List<float>();
		//get the dates
		List<DateTime> times = habit.CompletedInstances.Keys.ToList();
		//sort them
		times.Sort();

		

		for (int i = 0; i < times.Count; i++)
		{
			//Debug.Log(habit.SuccessPercentAtDate(times[i]));
			values.Add(habit.SuccessPercentAtDate(times[i]));
		}

		SpawnGraphItems(values, times);

	}

	private void SpawnGraphItems(List<float> values,List<DateTime> times)
	{
		foreach (var go in graphObjects)
		{
			Destroy(go);
		}
		graphObjects.Clear();


		float x = times.Count * 240;
		if (x < 1000f)
		{
			x = 1000f;
		}

		horizontal.sizeDelta = new Vector2(x, horizontal.sizeDelta.y);
		Debug.Log(horizontal.sizeDelta);

		float deltaX = horizontal.sizeDelta.x / times.Count;
		GameObject previousDot = null;
		float xOffset = 120f;
		for (int i = 0; i < times.Count; i++)
		{
			RectTransform xDash = Instantiate(dashTemplateX,horizontal);
			xDash.anchoredPosition = new Vector2(i * deltaX + xOffset, 0f);
			xDash.gameObject.SetActive(true);
			graphObjects.Add(xDash.gameObject);

			RectTransform xLabel = Instantiate(labelTemplateX, horizontal);
			xLabel.anchoredPosition = new Vector2(i * deltaX, 40f);
			xLabel.gameObject.SetActive(true);
			xLabel.GetComponent<Text>().text = times[i].ToShortDateString();
			graphObjects.Add(xLabel.gameObject);

			GameObject gameOb = new GameObject("dot", typeof(Image));
			gameOb.transform.SetParent(horizontal, false);
			gameOb.GetComponent<Image>().sprite = dotSprite;
			gameOb.GetComponent<Image>().color = habit.HabitColor;
			RectTransform rect = gameOb.GetComponent<RectTransform>();

			float y = values[i] / 100f * (horizontal.sizeDelta.y - 50);
			rect.anchoredPosition = new Vector2(i * deltaX + xOffset, y);
			rect.sizeDelta = new Vector2(20, 20);
			rect.anchorMin = new Vector2(0, 0);
			rect.anchorMax = new Vector2(0, 0);

			//TODO: move this to a separate loop
			RectTransform yDash = Instantiate(dashTemplateY, horizontal);
			yDash.anchoredPosition = new Vector2(0, y);
			yDash.gameObject.SetActive(true);
			graphObjects.Add(yDash.gameObject);
			//TODO: alter the size of this to extend all the way to the end (horizontal.sizeDelta.x)

			Tooltip tp = gameOb.AddComponent<Tooltip>();
			tp.message = values[i].ToString();
			tp.PointerEntered += ShowTooltip;
			tp.PointerExited += HideTooltip;

			GameObject connection = null;
			if (previousDot != null)
			{
				connection = CreateDotConnection(previousDot.GetComponent<RectTransform>().anchoredPosition,
					gameOb.GetComponent<RectTransform>().anchoredPosition);
				graphObjects.Add(connection);
			}

			previousDot = gameOb;

			graphObjects.Add(gameOb);
		}

	}

	private GameObject CreateDotConnection(Vector2 posA, Vector2 posB)
	{
		GameObject gameObject = new GameObject("dotConnection", typeof(Image));
		gameObject.transform.SetParent(horizontal, false);
		gameObject.GetComponent<Image>().raycastTarget = false;
		gameObject.GetComponent<Image>().color = habit.HabitColor;
		RectTransform rect = gameObject.GetComponent<RectTransform>();
		Vector2 dir = (posB - posA).normalized;
		float distance = Vector2.Distance(posA, posB);
		rect.anchoredPosition = posA + dir * distance * 0.5f;
		rect.sizeDelta = new Vector2(distance, 3);
		rect.anchorMin = new Vector2(0, 0);
		rect.anchorMax = new Vector2(0, 0);

		rect.localEulerAngles = new Vector3(0, 0, Mathf.Rad2Deg * Mathf.Atan2(dir.y, dir.x));

		return gameObject;
	}

	private void SetGraph()
	{
		streakViewer.ClearObjects();

		streakViewer.gameObject.SetActive(false);
		windowGraph.gameObject.SetActive(true);

		viewGraphButton.gameObject.SetActive(false);
		viewStreaksButton.gameObject.SetActive(true);

		List<float> values = new List<float>();
		//get the dates
		List<DateTime> times = habit.CompletedInstances.Keys.ToList();
		//sort them
		times.Sort();

		for (int i = 0; i < times.Count; i++)
		{
			//Debug.Log(habit.SuccessPercentAtDate(times[i]));
			values.Add(habit.SuccessPercentAtDate(times[i]));
		}

		windowGraph.SetData(values, times);
	}

	public void ShowTooltip(string text, Vector2 pos)
	{
		tooltipGameObject.SetActive(true);
		Text t = tooltipGameObject.transform.Find("text").GetComponent<Text>();
		t.text = text;
		Vector2 backgroundSize = new Vector2(t.preferredWidth + 8f, t.preferredHeight + 8f);
		tooltipGameObject.transform.Find("background").GetComponent<RectTransform>().sizeDelta = backgroundSize;
		tooltipGameObject.GetComponent<RectTransform>().anchoredPosition = pos;
		tooltipGameObject.transform.SetAsLastSibling();
	}

	public void HideTooltip(string text, Vector2 pos)
	{
		tooltipGameObject.SetActive(false);
	}

	protected void OnNextHabitClicked(Habit h)
	{

	}

	protected void OnPreviousHabitClicked(Habit h)
	{

	}

	protected void OnColorPickerButtonCliked()
	{
		//activate color picker
		colorBlocker.SetActive(true);
		colorPicker.gameObject.SetActive(true);
	}

	protected void OnColorPicked()
	{
		colorButton.image.color = colorPicker.TheColor;
		habit.SetColor(colorPicker.TheColor);
		MainMenu.instance.UpdateHabit(habit);
		colorBlocker.SetActive(false);
		colorPicker.gameObject.SetActive(false);
	}

	private void ToggleChanged()
	{
		//Debug.Log("changing");
		if (onDay.isOn)
		{
			switch ((RepititionType)repititionDropdown.value)
			{
				case RepititionType.SECONDLY:
				case RepititionType.MINUTELY:
				case RepititionType.HOURLY:
				case RepititionType.DAILY:
				case RepititionType.WEEKLY:
					//Debug.Log("jere");
					months.gameObject.SetActive(false);
					dayOfMonth.gameObject.SetActive(false);

					ordinalDropdown.gameObject.SetActive(false);
					month.gameObject.SetActive(false);
					daysOfWeek.gameObject.SetActive(false);
					break;
				case RepititionType.YEARLY:

					months.gameObject.SetActive(true);
					dayOfMonth.gameObject.SetActive(true);

					dayOfMonth.maxValue = DateTime.DaysInMonth(DateTime.Now.Year, (int)Enum.Parse(typeof(Months), months.options[0].text));

					ordinalDropdown.gameObject.SetActive(false);
					month.gameObject.SetActive(false);
					daysOfWeek.gameObject.SetActive(false);

					break;
				case RepititionType.MONTHLY:

					dayOfMonth.gameObject.SetActive(true);
					dayOfMonth.maxValue = 31;

					months.gameObject.SetActive(false);
					ordinalDropdown.gameObject.SetActive(false);
					month.gameObject.SetActive(false);
					daysOfWeek.gameObject.SetActive(false);
					break;
				default:
					break;
			}

		}
		else
		{
			switch ((RepititionType)repititionDropdown.value)
			{
				case RepititionType.SECONDLY:
				case RepititionType.MINUTELY:
				case RepititionType.HOURLY:
				case RepititionType.DAILY:
				case RepititionType.WEEKLY:
					//Debug.Log("here");
					months.gameObject.SetActive(false);
					dayOfMonth.gameObject.SetActive(false);

					ordinalDropdown.gameObject.SetActive(false);
					month.gameObject.SetActive(false);
					daysOfWeek.gameObject.SetActive(false);
					break;
				case RepititionType.YEARLY:

					months.gameObject.SetActive(false);
					dayOfMonth.gameObject.SetActive(false);

					ordinalDropdown.gameObject.SetActive(true);



					month.gameObject.SetActive(true);
					daysOfWeek.gameObject.SetActive(true);

					break;
				case RepititionType.MONTHLY:
					months.gameObject.SetActive(false);
					dayOfMonth.gameObject.SetActive(false);

					ordinalDropdown.gameObject.SetActive(true);

					daysOfWeek.gameObject.SetActive(true);

					month.gameObject.SetActive(false);

					break;
				default:
					break;
			}
		}
	}

	bool CheckWeekValidity()
	{
		int selected = 0;
		if (monday.isOn)
		{
			selected++;
		}

		if (tuesday.isOn)
		{
			selected++;
		}

		if (wednesday.isOn)
		{
			selected++;
		}

		if (thursday.isOn)
		{
			selected++;
		}

		if (friday.isOn)
		{
			selected++;
		}

		if (saturday.isOn)
		{
			selected++;
		}

		if (sunday.isOn)
		{
			selected++;
		}

		if (selected < 1)
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "At least one day must be selected";
		}

		return selected >= 1;
	}

	void InitializeRepititionType()
	{
		repititionDropdown.ClearOptions();
		repititionDropdown.onValueChanged.AddListener(RepititionTypeChanged);
		List<string> options = new List<string>();
		foreach (var item in Enum.GetValues(typeof(RepititionType)))
		{
			options.Add(item.ToString());
		}

		repititionDropdown.AddOptions(options);

		//repititionDropdown.value = 1;
		//repititionDropdown.value = 0;
		//ending.value = 1;
		//ending.value = 0;

		RecurrencePattern pattern = new RecurrencePattern(habit.RepititionRules[0]);
		//we should use this function to initialize all the values
		switch (pattern.Frequency)
		{
			case Ical.Net.FrequencyType.None:
				break;
			case Ical.Net.FrequencyType.Secondly:
				repititionDropdown.value = (int)RepititionType.SECONDLY;
				break;
			case Ical.Net.FrequencyType.Minutely:
				repititionDropdown.value = (int)RepititionType.MINUTELY;
				break;
			case Ical.Net.FrequencyType.Hourly:
				repititionDropdown.value = (int)RepititionType.HOURLY;
				break;
			case Ical.Net.FrequencyType.Daily:
				repititionDropdown.value = (int)RepititionType.DAILY;
				break;
			case Ical.Net.FrequencyType.Weekly:
				repititionDropdown.value = (int)RepititionType.WEEKLY;
				foreach (var item in pattern.ByDay)
				{
					switch (item.DayOfWeek)
					{
						case DayOfWeek.Friday:
							friday.isOn = true;
							break;
						case DayOfWeek.Monday:
							monday.isOn = true;
							break;
						case DayOfWeek.Saturday:
							saturday.isOn = true;
							break;
						case DayOfWeek.Sunday:
							sunday.isOn = true;
							break;
						case DayOfWeek.Thursday:
							thursday.isOn = true;
							break;
						case DayOfWeek.Tuesday:
							tuesday.isOn = true;
							break;
						case DayOfWeek.Wednesday:
							wednesday.isOn = true;
							break;
						default:
							break;
					}
				}
				break;
			case Ical.Net.FrequencyType.Monthly:
				repititionDropdown.value = (int)RepititionType.MONTHLY;
				if (pattern.ByMonthDay.Count != 0)
				{
					onDay.isOn = true;
					dayOfMonth.value = pattern.ByMonthDay[0];
				}
				else
				{
					onThe.isOn = true;
					ordinalDropdown.value = pattern.BySetPosition[0];
					daysOfWeek.value = (int)pattern.ByDay[0].DayOfWeek;
				}
				break;
			case Ical.Net.FrequencyType.Yearly:
				repititionDropdown.value = (int)RepititionType.YEARLY;
				if (pattern.ByMonthDay.Count != 0)
				{
					onDay.isOn = true;
					months.value = pattern.ByMonth[0] - 1;
					dayOfMonth.value = pattern.ByMonthDay[0];
				}
				else
				{
					onThe.isOn = true;
					ordinalDropdown.value = pattern.BySetPosition[0];
					month.value = pattern.ByMonth[0] - 1;
					daysOfWeek.value = (int)pattern.ByDay[0].DayOfWeek;
				}
				break;
			default:
				break;
		}

		interval.text = pattern.Interval.ToString();

		if (pattern.Count != int.MinValue)
		{
			ending.value = (int)EndType.AFTER;
			count.text = pattern.Count.ToString();
		}
		else if (pattern.Until != DateTime.MinValue)
		{
			ending.value = (int)EndType.ON;
			endYearInput.text = pattern.Until.Year.ToString();
			endMonthInput.text = pattern.Until.Month.ToString();
			endDayInput.text = pattern.Until.Day.ToString();
		}
		else
		{
			ending.value = (int)EndType.NEVER;
			endDateEntryHolder.SetActive(false);
			count.gameObject.SetActive(false);
		}

		if (pattern.ByHour.Count > 0)
		{
			timeHourInput.text = pattern.ByHour[0].ToString("D2");
		}

		if (pattern.ByMinute.Count > 0)
		{
			timeMinuteInput.text = pattern.ByMinute[0].ToString("D2");
		}

	}

	void RepititionTypeChanged(int x)
	{
		//Debug.Log(((RepititionType)x).ToString());

		switch ((RepititionType)x)
		{
			case RepititionType.SECONDLY:
				interval.transform.Find("IntervalPeriodName").GetComponent<Text>().text = "second (s)";
				break;
			case RepititionType.MINUTELY:
				interval.transform.Find("IntervalPeriodName").GetComponent<Text>().text = "minute (s)";
				break;
			case RepititionType.HOURLY:
				interval.transform.Find("IntervalPeriodName").GetComponent<Text>().text = "hour (s)";
				break;
			case RepititionType.DAILY:
				interval.transform.Find("IntervalPeriodName").GetComponent<Text>().text = "day (s)";
				break;
			case RepititionType.WEEKLY:
				interval.transform.Find("IntervalPeriodName").GetComponent<Text>().text = "week (s)";
				break;
			case RepititionType.MONTHLY:
				interval.transform.Find("IntervalPeriodName").GetComponent<Text>().text = "month (s)";
				break;
			case RepititionType.YEARLY:
				interval.transform.Find("IntervalPeriodName").GetComponent<Text>().text = "year (s)";
				break;
			default:
				break;
		}
		if ((RepititionType)x == RepititionType.WEEKLY)
		{
			weeklyHolder.SetActive(true);
		}
		else
		{
			weeklyHolder.SetActive(false);
		}

		switch ((RepititionType)x)
		{
			case RepititionType.SECONDLY:
			case RepititionType.MINUTELY:
			case RepititionType.HOURLY:
				timeEntryHolder.SetActive(false);
				break;
			case RepititionType.DAILY:
			case RepititionType.WEEKLY:
			case RepititionType.MONTHLY:
			case RepititionType.YEARLY:
				timeEntryHolder.SetActive(true);
				break;
			default:
				break;
		}

		SetToggles((RepititionType)x);
		SetOrdinal((RepititionType)x);
	}

	void EndingChanged(int x)
	{
		//Debug.Log("Changing ending");
		switch ((EndType)x)
		{
			case EndType.NEVER:
				count.gameObject.SetActive(false);
				endDateEntryHolder.SetActive(false);
				break;
			case EndType.AFTER:
				endDateEntryHolder.SetActive(false);
				count.gameObject.SetActive(true);
				break;
			case EndType.ON:
				count.gameObject.SetActive(false);
				endDateEntryHolder.SetActive(true);
				break;
		}
	}

	bool IsValidTime()
	{
		int hour, minute;
		Int32.TryParse(timeHourInput.text, out hour);
		if (hour >= 0 && hour < 24)
		{
			Int32.TryParse(timeMinuteInput.text, out minute);
			if (minute >= 0 && minute < 60)
			{
				return true;
			}
		}

		return false;
	}

	bool IsValidEndDate()
	{
		int year, month, day;
		Int32.TryParse(endYearInput.text, out year);
		if (year >= 0)
		{
			Int32.TryParse(endMonthInput.text, out month);
			if (month >= 1 && month <= 12)
			{
				Int32.TryParse(endDayInput.text, out day);
				if (day >= 1 && day <= DateTime.DaysInMonth(year, month))
				{
					return true;
				}
			}
		}

		return false;
	}

	void OnOrdinalChanged(int x)
	{
		//Debug.Log("int: " + x);
		//Debug.Log("enum: " + (Enum.Parse(typeof(Ordinal),ordinalDropdown.options[x].text)).ToString());
	}

	bool IsValidCount()
	{
		int c;
		Int32.TryParse(count.text, out c);
		return c >= 1;
	}

	bool IsValidName()
	{
		return !habitName.text.Equals("");
	}

	void SetOrdinal(RepititionType rt)
	{
		switch (rt)
		{
			case RepititionType.SECONDLY:
			case RepititionType.MINUTELY:
			case RepititionType.HOURLY:
			case RepititionType.DAILY:
			case RepititionType.WEEKLY:
				ordinalDropdown.gameObject.SetActive(false);
				break;
			case RepititionType.YEARLY:
			case RepititionType.MONTHLY:

				//ordinalDropdown.gameObject.SetActive(true);

				onThe.gameObject.SetActive(true);
				onDay.gameObject.SetActive(true);
				break;
			default:
				break;
		}

	}

	void SetToggles(RepititionType rt)
	{
		//Debug.Log("setting toggles");
		switch (rt)
		{
			case RepititionType.SECONDLY:
			case RepititionType.MINUTELY:
			case RepititionType.HOURLY:
			case RepititionType.DAILY:
			case RepititionType.WEEKLY:
				//Debug.Log("not visible");
				onThe.gameObject.SetActive(false);
				onDay.gameObject.SetActive(false);

				months.gameObject.SetActive(false);
				dayOfMonth.gameObject.SetActive(false);

				ordinalDropdown.gameObject.SetActive(false);
				month.gameObject.SetActive(false);
				daysOfWeek.gameObject.SetActive(false);
				break;
			case RepititionType.YEARLY:
			case RepititionType.MONTHLY:
				//Debug.Log("visible");
				onThe.gameObject.SetActive(true);
				onDay.isOn = false;
				onDay.isOn = true;
				onDay.gameObject.SetActive(true);
				break;
			default:
				break;
		}
	}

	// Update is called once per frame
	void Update()
    {
        
    }

	public void OnDisable()
	{
		((Text)habitNameInput.placeholder).text = "Enter text";
		habitNameInput.text = "";

		((Text)endYearInput.placeholder).text = "Enter text";
		endYearInput.text = "";

		((Text)endMonthInput.placeholder).text = "Enter text";
		endMonthInput.text = "";

		((Text)endDayInput.placeholder).text = "Enter text";
		endDayInput.text = "";

		((Text)timeHourInput.placeholder).text = "Enter text";
		timeHourInput.text = "";

		((Text)timeMinuteInput.placeholder).text = "Enter text";
		timeMinuteInput.text = "";

		((Text)count.placeholder).text = "Enter text";
		count.text = "";

		((Text)length.placeholder).text = "Enter text";
		length.text = "";

		ending.value = 0;
		repititionDropdown.value = 0;

		onDay.isOn = true;
		months.value = 0;
		dayOfMonth.value = 1;

		ordinalDropdown.value = 0;
		daysOfWeek.value = 0;
		month.value = 0;

		monday.isOn = true;
		tuesday.isOn = false;
		wednesday.isOn = false;
		thursday.isOn = false;
		friday.isOn = false;
		saturday.isOn = false;
		sunday.isOn = false;

		unitName.value = 0;

		((Text)interval.placeholder).text = "Enter text";
		interval.text = "1";

		errorText.text = "";
		errorText.gameObject.SetActive(false);

		colorButton.onClick.RemoveAllListeners();
		acceptColorButton.onClick.RemoveAllListeners();

		colorPicker.TheColor = new Color(0f, 0f, 0f, 0f);
	}
}
