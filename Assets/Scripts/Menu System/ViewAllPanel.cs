﻿using Ical.Net.DataTypes;
using Ical.Net.Interfaces.DataTypes;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ViewAllPanel : MonoBehaviour
{
	public Transform habitHolder;
	public GameObject habitPrefab;

	Dictionary<string, GameObject> habitObjects = new Dictionary<string, GameObject>();
	Dictionary<string, Habit> habitDictionary = new Dictionary<string, Habit>();

	public delegate void HabitHandler(string id);
	//public event HabitHandler HabitRemoved;
	public event HabitHandler HabitDetailsViewed;

	public void SpawnHabits(List<Habit> habits)
	{
		ClearList();
		foreach (var habit in habits)
		{
			GameObject habitObject = Instantiate(habitPrefab, habitHolder);
			AllHabitsObject allHabitsObject = habitObject.GetComponent<AllHabitsObject>();
			// set the data
			allHabitsObject.habitName.text = habit.HabitName;
			RecurrencePattern pattern = new RecurrencePattern(habit.RepititionRules[0]);
			allHabitsObject.habitPattern.text = pattern.ConvertToSentence();

			allHabitsObject.habitSuccess.text = habit.SuccessPercentage.ToString() + "%";
			allHabitsObject.habitStreak.text = habit.LongestStreak.ToString();

			habitObject.GetComponent<Image>().color = habit.HabitColor;

			//set the delegates
			allHabitsObject.removeHabitButton.onClick.AddListener(delegate { DeleteHabit(habit.HabitID); });
			//habitObject.GetComponent<Button>().onClick.AddListener(delegate { ViewHabitDetail(habit.HabitID); });
			MainMenu.instance.RegisterHabitFromViewAll(habit.HabitID, habitObject.GetComponent<Button>(), allHabitsObject.removeHabitButton);
			//HabitDetailsViewed += MainMenu.instance.ViewHabitDetails;
			//HabitRemoved += MainMenu.instance.

			//add to dictionaries
			habitObjects.Add(habit.HabitID, habitObject);
			habitDictionary.Add(habit.HabitID, habit);

		}
	}

	public void ClearList()
	{
		List<string> keys = habitObjects.Keys.ToList();

		for (int i = 0; i < keys.Count; i++)
		{
			Destroy(habitObjects[keys[i]]);
			habitDictionary.Remove(keys[i]);
		}

		keys.Clear();
		habitObjects.Clear();
		habitDictionary.Clear();
	}

	protected void ViewHabitDetail(string id)
	{
		if (HabitDetailsViewed != null)
		{
			HabitDetailsViewed(id);
		}
	}

	protected void DeleteHabit(string id)
	{
		Destroy(habitObjects[id]);
		habitObjects.Remove(id);
		habitDictionary.Remove(id);
	}
}
