﻿using UnityEngine;
using UnityEngine.UI;

public class TextUpdater : MonoBehaviour
{
	public Text text;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public void OnIntChanged(float x)
	{
		text.text = x.ToString();
	}
}
