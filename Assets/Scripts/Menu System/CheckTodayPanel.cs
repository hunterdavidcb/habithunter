﻿using Ical.Net.DataTypes;
using Ical.Net.Interfaces.DataTypes;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CheckTodayPanel : MonoBehaviour
{
	public Transform habitHolder;
	public GameObject todayhabitPrefab;
	Dictionary<string, List<GameObject>> habitObjects = new Dictionary<string, List<GameObject>>();

	//public 
	public delegate void CheckTodayHandler();
	public event CheckTodayHandler CheckFinished;

	public void SpawnHabits(Dictionary<Habit, List<IPeriod>> habits)
	{
		ClearList();

		//Debug.Log("here");
		//Debug.Log(habits.Count);
		foreach (var hab in habits)
		{
			string x = hab.Key.HabitID;
			var pa = new RecurrencePattern(hab.Key.RepititionRules[0]);
			//Debug.Log(pa.ToString());
			foreach (var period in hab.Value)
			{
				string time = period.StartTime.Hour.ToString("D2") + ":" + period.StartTime.Minute.ToString("D2");
				//string time = pa.ByHour[0].ToString("D2") + ":" + pa.ByMinute[0].ToString("D2");
				GameObject habit = Instantiate(todayhabitPrefab, habitHolder);
				habit.GetComponent<TodayHabit>().message.text = habit.GetComponent<TodayHabit>().question.Replace("***", hab.Key.HabitName + " at " + time);
				habit.GetComponent<Image>().color = new Color(hab.Key.HabitColorR, hab.Key.HabitColorG, hab.Key.HabitColorB);

				if (habitObjects.ContainsKey(x))
				{
					habitObjects[x].Add(habit);
				}
				else
				{
					habitObjects.Add(x, new List<GameObject>() { habit });
				}

				habit.GetComponent<TodayHabit>().yesButton.onClick.AddListener(() => OnYesClicked(x,habitObjects[x].IndexOf(habit)));
				habit.GetComponent<TodayHabit>().noButton.onClick.AddListener(() => OnNoClicked(x, habitObjects[x].IndexOf(habit)));

				MainMenu.instance.RegisterCheckInHabit(habit.GetComponent<TodayHabit>(), x, period.StartTime.Value);
			}
		}
	}

	void OnNoClicked(string i, int index)
	{
		Destroy(habitObjects[i][index]);
		habitObjects[i].RemoveAt(index);
		if (habitObjects[i].Count == 0)
		{
			habitObjects.Remove(i);
		}

		if (habitObjects.Count == 0)
		{
			if (CheckFinished != null)
			{
				CheckFinished();
			}
		}
	}

	void OnYesClicked(string i, int index)
	{
		Destroy(habitObjects[i][index]);
		habitObjects[i].RemoveAt(index);
		if (habitObjects[i].Count == 0)
		{
			habitObjects.Remove(i);
		}

		if (habitObjects.Count == 0)
		{
			if (CheckFinished != null)
			{
				CheckFinished();
			}
		}
	}

	public void ClearList()
	{
		List<string> keys = habitObjects.Keys.ToList();

		for (int i = 0; i < keys.Count; i++)
		{
			//avoid out of range errors
			for (int x = habitObjects[keys[i]].Count -1 ; x > -1 ; x--)
			{
				Destroy(habitObjects[keys[i]][x]);
			}
		}

		keys.Clear();
		habitObjects.Clear();
	}
}
