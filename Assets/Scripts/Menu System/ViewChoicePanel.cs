﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewChoicePanel : MonoBehaviour
{
	public Button viewTodayButton;
	public Button viewAllButton;
	public Button viewMonthButton;
	public Button viewWeekButton;
	public Button viewDayButton;
}
