﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class StreakViewer : MonoBehaviour
{
	public Sprite ring;
	public Sprite circle;
	public GameObject line;

	int maxX = 7;
	int maxY = 5;

	float width;
	float height;
	RectTransform graph;
	RectTransform labelTemplateX;

	Dictionary<DateTime, List<GameObject>> habitStreakObjects = new Dictionary<DateTime, List<GameObject>>();
    // Start is called before the first frame update
    void OnEnable()
    {
		width = transform.Find("graphContainer").GetComponent<RectTransform>().sizeDelta.x;
		height = transform.Find("graphContainer").GetComponent<RectTransform>().sizeDelta.y;
		graph = transform.Find("graphContainer").GetComponent<RectTransform>();
		labelTemplateX = graph.Find("labelTemplateX").GetComponent<RectTransform>();
	}

    // Update is called once per frame
    void Update()
    {
        
    }

	public void SetStreaks(Dictionary<DateTime,HabitStreak > streaks, Color habitColor)
	{
		ClearObjects();
		Debug.Log("called");
		foreach (var item in streaks)
		{
			//we can show it on one line
			if (item.Value.Instances.Count < maxX)
			{
				RectTransform previousPos = null;
				for (int i = 0; i < item.Value.Instances.Count; i++)
				{
					Debug.Log("inside");
					GameObject go = new GameObject("ring", typeof(Image));
					go.GetComponent<Image>().sprite = ring;
					go.GetComponent<Image>().color = habitColor;
					go.transform.SetParent(graph,false);
					RectTransform rect = go.GetComponent<RectTransform>();
					rect.anchoredPosition = new Vector2((width / (maxX + 5)) + ((width / maxX)* i), height / 2);
					rect.anchorMin = new Vector2(0, 0);
					rect.anchorMax = new Vector2(0, 0);

					if (!habitStreakObjects.ContainsKey(item.Key))
					{
						habitStreakObjects.Add(item.Key, new List<GameObject>() { go });
					}
					else
					{
						habitStreakObjects[item.Key].Add(go);
					}

					go = new GameObject("circle", typeof(Image));
					go.GetComponent<Image>().sprite = circle;
					go.GetComponent<Image>().color = new Color(habitColor.r,habitColor.g, habitColor.b, 0.5f);
					go.transform.SetParent(graph, false);
					rect = go.GetComponent<RectTransform>();
					rect.anchoredPosition = new Vector2((width / (maxX + 5)) + ((width / maxX) * i), height / 2);
					rect.anchorMin = new Vector2(0, 0);
					rect.anchorMax = new Vector2(0, 0);

					if (previousPos != null)
					{
						//we need to make a connection to the previous
						GameObject l = CreateDotConnection(rect.anchoredPosition, previousPos.anchoredPosition, habitColor);
						if (habitStreakObjects[item.Key] == null)
						{
							habitStreakObjects.Add(item.Key, new List<GameObject>() { l });
						}
						else
						{
							habitStreakObjects[item.Key].Add(l);
						}
					}

					if (habitStreakObjects[item.Key] == null)
					{
						habitStreakObjects.Add(item.Key, new List<GameObject>() { go });
					}
					else
					{
						habitStreakObjects[item.Key].Add(go);
					}

					RectTransform labelX = Instantiate(labelTemplateX);

					if (habitStreakObjects[item.Key] == null)
					{
						habitStreakObjects.Add(item.Key, new List<GameObject>() { labelX.gameObject });
					}
					else
					{
						habitStreakObjects[item.Key].Add(labelX.gameObject);
					}

					labelX.SetParent(graph, false);
					labelX.gameObject.SetActive(true);
					labelX.anchoredPosition = rect.anchoredPosition;
					//test this
					labelX.GetComponent<Text>().text = item.Value.Instances.ToList()[i].ToShortDateString();

					previousPos = rect;
				}
			}
			else
			{
				//we need to split it over several lines
				// get the number of lines needed
				int lineCount = Mathf.CeilToInt((float)item.Value.Instances.Count / maxX);
				Debug.Log(lineCount);
				for (int i = 0; i < lineCount; i++)
				{
					Debug.Log((i + 1) * maxX);
					int perLine = i < lineCount - 1 ? maxX : item.Value.Instances.Count - (i * maxX);//((i + 1) * maxX) < item.Value.Instances.Count?
					//	(item.Value.Instances.Count - (i + 1) * maxX) % maxX 
					//	: item.Value.Instances.Count % (i * maxX);
					Debug.Log(perLine);
					// item.Value.Instances.Count 
					RectTransform previousPos = null;
					for (int x = 0; x < perLine; x++)
					{
						//Debug.Log("inside");
						GameObject go = new GameObject("ring", typeof(Image));
						go.GetComponent<Image>().sprite = ring;
						go.GetComponent<Image>().color = habitColor;
						go.transform.SetParent(graph, false);
						RectTransform rect = go.GetComponent<RectTransform>();
						rect.anchoredPosition = new Vector2((width / (maxX + 5)) + ((width / maxX) * x),
							height -50f - (height / lineCount) * i);
						rect.anchorMin = new Vector2(0, 0);
						rect.anchorMax = new Vector2(0, 0);

						if (!habitStreakObjects.ContainsKey(item.Key))
						{
							habitStreakObjects.Add(item.Key, new List<GameObject>() { go });
						}
						else
						{
							habitStreakObjects[item.Key].Add(go);
						}

						go = new GameObject("circle", typeof(Image));
						go.GetComponent<Image>().sprite = circle;
						go.GetComponent<Image>().color = new Color(habitColor.r, habitColor.g, habitColor.b, 0.5f);
						go.transform.SetParent(graph, false);
						rect = go.GetComponent<RectTransform>();
						rect.anchoredPosition = new Vector2((width / (maxX + 5)) + ((width / maxX) * x),
							height - 50f - (height / lineCount) * i);
						rect.anchorMin = new Vector2(0, 0);
						rect.anchorMax = new Vector2(0, 0);

						if (previousPos != null)
						{
							//we need to make a connection to the previous
							GameObject l = CreateDotConnection(rect.anchoredPosition, previousPos.anchoredPosition,habitColor);
							if (habitStreakObjects[item.Key] == null)
							{
								habitStreakObjects.Add(item.Key, new List<GameObject>() { l });
							}
							else
							{
								habitStreakObjects[item.Key].Add(l);
							}
						}

						if (habitStreakObjects[item.Key] == null)
						{
							habitStreakObjects.Add(item.Key, new List<GameObject>() { go });
						}
						else
						{
							habitStreakObjects[item.Key].Add(go);
						}

						RectTransform labelX = Instantiate(labelTemplateX);

						if (habitStreakObjects[item.Key] == null)
						{
							habitStreakObjects.Add(item.Key, new List<GameObject>() { labelX.gameObject });
						}
						else
						{
							habitStreakObjects[item.Key].Add(labelX.gameObject);
						}

						labelX.SetParent(graph, false);
						labelX.gameObject.SetActive(true);
						labelX.anchoredPosition = rect.anchoredPosition;
						//test this
						labelX.GetComponent<Text>().text = item.Value.Instances.ToList()[i * maxX + x].ToShortDateString();

						previousPos = rect;
					}
				}
				
			}
		}
	}

	private GameObject CreateDotConnection(Vector2 posA, Vector2 posB, Color habitColor)
	{
		GameObject gameObject = Instantiate(line); //new GameObject("dotConnection", typeof(Image));
		gameObject.transform.SetParent(graph, false);
		gameObject.GetComponent<Image>().raycastTarget = false;
		gameObject.GetComponent<Image>().color = new Color(habitColor.r, habitColor.g, habitColor.b, 0.75f);
		RectTransform rect = gameObject.GetComponent<RectTransform>();
		Vector2 dir = (posB - posA).normalized;
		float distance = Vector2.Distance(posA, posB);
		rect.anchoredPosition = posA + dir * distance * 0.5f;
		rect.sizeDelta = new Vector2(distance - 85, 20);
		rect.anchorMin = new Vector2(0, 0);
		rect.anchorMax = new Vector2(0, 0);

		rect.localEulerAngles = new Vector3(0, 0, Mathf.Rad2Deg * Mathf.Atan2(dir.y, dir.x));

		return gameObject;
	}

	public void ClearObjects()
	{
		foreach (var item in habitStreakObjects)
		{
			foreach (var ob in item.Value)
			{
				Destroy(ob);
			}
			item.Value.Clear();
		}

		habitStreakObjects.Clear();
	}
}
