﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HitTest : MonoBehaviour
{
	public delegate void ClickHandler(GameObject self);
	public event ClickHandler ClickFinished;

	private void Awake()
	{
		GetComponent<Image>().alphaHitTestMinimumThreshold = .1f;
	}
	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public void Finished()
	{
		//Debug.Log("here");
		if (ClickFinished != null)
		{
			ClickFinished(gameObject);
		}
	}
}
