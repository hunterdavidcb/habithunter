﻿using Ical.Net;
using Ical.Net.DataTypes;
using Ical.Net.Interfaces.DataTypes;
using Ical.Net.Serialization.iCalendar.Serializers.DataTypes;

using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class CreateHabitMenu : MonoBehaviour
{
	public GameObject panel;
	public Dropdown repititionDropdown;
	public Dropdown ordinalDropdown;
	public Dropdown months;
	public Dropdown daysOfWeek;
	public Dropdown ending;
	public Dropdown month;
	public Slider dayOfMonth;
	public InputField interval, count, length;
	public Text intervalPeriodName;
	public Dropdown unitName;
	public InputField habitName;
	public Toggle hasLength;
	public GameObject lengthEntry;
	public Toggle onThe;
	public Toggle onDay;
	public GameObject endDateEntryHolder, startDateEntryHolder, timeEntryHolder;
	public GameObject weeklyHolder;
	public Text errorText;
	public Button colorButton;
	public Button acceptColorButton;
	public ColorPickerTriangle colorPicker;
	public GameObject colorBlocker;
	public GameObject calendarPanel;
	public Button pickFromCalendarStartButton;
	public Button pickFromCalendarEndButton;

	InputField endYearInput, endMonthInput, endDayInput;
	InputField startYearInput, startMonthInput, startDayInput;
	InputField timeHourInput, timeMinuteInput;
	Toggle monday, tuesday, wednesday, thursday, friday, saturday, sunday;

	GameObject onDayHolder,onTheHolder, intervalHolder, endingHolder, countHolder;

	public delegate void HabitHandler(Habit h);
	public event HabitHandler HabitCreated;
	//public event HabitHandler HabitRemoved;

	Color habitColor = new Color(0f, 0f, 0f, 0f);

	private void OnEnable()
	{
		colorButton.onClick.AddListener(OnColorPickerButtonCliked);
		acceptColorButton.onClick.AddListener(OnColorPicked);

		pickFromCalendarStartButton.onClick.AddListener(OnCalendarStartClicked);
	}

	#region Calendar Handling

	private void OnCalendarStartClicked()
	{
		//activate the calendar panel
		calendarPanel.SetActive(true);
		//activiate the monthly panel
		calendarPanel.GetComponent<CalendarPanel>().
			monthlyPanel.SetActive(true);

		//this is so fucking cool!!!!
		MonthPanel.MonthDayHandler monthdayCallback = new MonthPanel.MonthDayHandler(MonthDayStartCallbackFuntion);
		//initialize the month
		MonthPanel monthPanel = calendarPanel.GetComponent<CalendarPanel>().
			monthlyPanel.GetComponent<MonthPanel>();
		monthPanel.InitializeMonth(DateTime.Today.Year, DateTime.Today.Month, monthdayCallback);
		monthPanel.previousMonthButton.onClick.RemoveAllListeners();
		monthPanel.nextMonthButton.onClick.RemoveAllListeners();

		monthPanel.previousMonthButton.onClick.AddListener(delegate
		{ OnPreviousMonthButtonPressed(DateTime.Today.Month, DateTime.Today.Year); });
		monthPanel.nextMonthButton.onClick.AddListener(delegate
		{ OnNextMonthButtonPressed(DateTime.Today.Month, DateTime.Today.Year); });
		//throw new NotImplementedException();
	}

	private void OnCalendarEndClicked()
	{
		//activate the calendar panel
		calendarPanel.SetActive(true);
		//activiate the monthly panel
		calendarPanel.GetComponent<CalendarPanel>().
			monthlyPanel.SetActive(true);

		//this is so fucking cool!!!!
		MonthPanel.MonthDayHandler monthdayCallback = new MonthPanel.MonthDayHandler(MonthDayEndCallbackFuntion);
		//initialize the month
		calendarPanel.GetComponent<CalendarPanel>().
			monthlyPanel.GetComponent<MonthPanel>().InitializeMonth(DateTime.Today.Year, DateTime.Today.Month, monthdayCallback);
		MonthPanel monthPanel = calendarPanel.GetComponent<CalendarPanel>().
			monthlyPanel.GetComponent<MonthPanel>();
		monthPanel.previousMonthButton.onClick.RemoveAllListeners();
		monthPanel.nextMonthButton.onClick.RemoveAllListeners();

		monthPanel.previousMonthButton.onClick.AddListener(delegate
		{ OnPreviousMonthButtonPressed(DateTime.Today.Month, DateTime.Today.Year); });
		monthPanel.nextMonthButton.onClick.AddListener(delegate
		{ OnNextMonthButtonPressed(DateTime.Today.Month, DateTime.Today.Year); });
		//throw new NotImplementedException();
	}

	private void OnNextMonthButtonPressed(int currentMonth, int currentYear)
	{
		MonthPanel monthPanel = calendarPanel.GetComponent<CalendarPanel>().
			monthlyPanel.GetComponent<MonthPanel>();
		monthPanel.nextMonthButton.onClick.RemoveAllListeners();

		//do sanity check on month number
		int nextYear = currentMonth < 12 ? currentYear : currentYear + 1;
		int nextMonth = currentMonth < 12 ? currentMonth + 1 : 1;
		//Debug.Log(nextYear);
		//Debug.Log(nextMonth);

		monthPanel.nextMonthButton.onClick.AddListener(delegate
		{ OnNextMonthButtonPressed(nextMonth, nextYear); });

		//remember to update the previous month button
		monthPanel.previousMonthButton.onClick.RemoveAllListeners();
		monthPanel.previousMonthButton.onClick.AddListener(delegate
		{ OnPreviousMonthButtonPressed(nextMonth, nextYear); });


		//update the text
		monthPanel.InitializeMonth(nextYear, nextMonth);
	}

	private void OnPreviousMonthButtonPressed(int currentMonth, int currentYear)
	{
		MonthPanel monthPanel = calendarPanel.GetComponent<CalendarPanel>().
			monthlyPanel.GetComponent<MonthPanel>();
		monthPanel.previousMonthButton.onClick.RemoveAllListeners();

		//do sanity check on month number
		int previousYear = currentMonth > 1 ? currentYear : currentYear - 1;
		int previousMonth = currentMonth > 1 ? currentMonth - 1 : 12;

		//Debug.Log(previousYear);
		//Debug.Log(previousMonth);

		monthPanel.previousMonthButton.onClick.AddListener(delegate
		{ OnPreviousMonthButtonPressed(previousMonth, previousYear); });

		//remember to update the next month button
		monthPanel.nextMonthButton.onClick.RemoveAllListeners();
		monthPanel.nextMonthButton.onClick.AddListener(delegate
		{ OnNextMonthButtonPressed(previousMonth, previousYear); });


		//update the text
		monthPanel.InitializeMonth(previousYear, previousMonth);
	}

	void MonthDayStartCallbackFuntion(int year, int month, int day)
	{
		//Debug.Log("here here here");
		//close the calendar panel
		calendarPanel.SetActive(false);
		//close the month panel
		calendarPanel.GetComponent<CalendarPanel>().
			monthlyPanel.SetActive(false);

		// check validity of date
		if (day <= DateTime.DaysInMonth(year, month) && day > 0)
		{
			//set values of start date input
			startYearInput.text = year.ToString();
			startMonthInput.text = month.ToString();
			startDayInput.text = day.ToString();
		}
		else
		{
			//show the error message
			errorText.gameObject.SetActive(true);
			errorText.text = "Please pick a valid start date.";
			return;
		}

	}

	void MonthDayEndCallbackFuntion(int year, int month, int day)
	{
		//Debug.Log("here here here");
		//close the calendar panel
		calendarPanel.SetActive(false);
		//close the month panel
		calendarPanel.GetComponent<CalendarPanel>().
			monthlyPanel.SetActive(false);

		// check validity of date
		if (day <= DateTime.DaysInMonth(year, month) && day > 0)
		{
			//set values of end date input
			endYearInput.text = year.ToString();
			endMonthInput.text = month.ToString();
			endDayInput.text = day.ToString();
		}
		else
		{
			//show the error message
			errorText.gameObject.SetActive(true);
			errorText.text = "Please pick a valid start date.";
			return;
		}
	}

	#endregion

	// Start is called before the first frame update
	void Start()
	{
		//colorButton.onClick.AddListener(OnColorPickerButtonCliked);
		//acceptColorButton.onClick.AddListener(OnColorPicked);

		colorPicker.gameObject.SetActive(false);
		colorBlocker.SetActive(false);

		errorText.gameObject.SetActive(false);
		//panel.SetActive(false);

		endYearInput = endDateEntryHolder.transform.GetChild(0).GetComponent<InputField>();
		endMonthInput = endDateEntryHolder.transform.GetChild(1).GetComponent<InputField>();
		endDayInput = endDateEntryHolder.transform.GetChild(2).GetComponent<InputField>();


		startYearInput = startDateEntryHolder.transform.GetChild(0).GetComponent<InputField>();
		startMonthInput = startDateEntryHolder.transform.GetChild(1).GetComponent<InputField>();
		startDayInput = startDateEntryHolder.transform.GetChild(2).GetComponent<InputField>();

		

		timeHourInput = timeEntryHolder.transform.GetChild(0).GetComponent<InputField>();
		timeMinuteInput = timeEntryHolder.transform.GetChild(1).GetComponent<InputField>();

		monday = weeklyHolder.transform.GetChild(0).GetComponent<Toggle>();
		tuesday = weeklyHolder.transform.GetChild(1).GetComponent<Toggle>();
		wednesday = weeklyHolder.transform.GetChild(2).GetComponent<Toggle>();
		thursday = weeklyHolder.transform.GetChild(3).GetComponent<Toggle>();
		friday = weeklyHolder.transform.GetChild(4).GetComponent<Toggle>();
		saturday = weeklyHolder.transform.GetChild(5).GetComponent<Toggle>();
		sunday = weeklyHolder.transform.GetChild(6).GetComponent<Toggle>();

		onDayHolder = onDay.transform.parent.gameObject;
		onTheHolder = onThe.transform.parent.gameObject;
		intervalHolder = interval.transform.parent.gameObject;
		countHolder = count.transform.parent.gameObject;

		List<string> options = new List<string>();
		onDay.onValueChanged.AddListener(delegate { ToggleChanged(); });
		onThe.onValueChanged.AddListener(delegate { ToggleChanged(); });

		

		//Debug.Log(ending.onValueChanged.GetPersistentTarget(0).name);

		foreach (var item in Enum.GetValues(typeof(EndType)))
		{
			options.Add(item.ToString());
		}

		ending.ClearOptions();
		ending.AddOptions(options);

		months.ClearOptions();
		options = new List<string>();
		foreach (var mon in Enum.GetValues(typeof(Months)))
		{
			options.Add(mon.ToString());
		}
		months.AddOptions(options);

		month.ClearOptions();
		options = new List<string>();
		foreach (var mon in Enum.GetValues(typeof(Months)))
		{
			options.Add(mon.ToString());
		}
		month.AddOptions(options);

		daysOfWeek.ClearOptions();
		options = new List<string>();
		foreach (var day in Enum.GetValues(typeof(DayOfWeek)))
		{
			options.Add(day.ToString());
		}
		daysOfWeek.AddOptions(options);

		ordinalDropdown.ClearOptions();
		options = new List<string>();
		foreach (var item in Enum.GetValues(typeof(Ordinal)))
		{
			options.Add(item.ToString());
		}

		ordinalDropdown.AddOptions(options);
		ordinalDropdown.onValueChanged.AddListener(OnOrdinalChanged);

		unitName.ClearOptions();
		options = new List<string>();
		foreach (var item in Enum.GetValues(typeof(RepititionUnitType)))
		{
			options.Add(item.ToString());
		}

		unitName.AddOptions(options);
		unitName.onValueChanged.AddListener(delegate { OnRepititionUnitChanged(); });

		ending.onValueChanged.AddListener(EndingChanged);

		monday.onValueChanged.AddListener(delegate { CheckWeekValidity(); });
		tuesday.onValueChanged.AddListener(delegate { CheckWeekValidity(); });
		wednesday.onValueChanged.AddListener(delegate { CheckWeekValidity(); });
		thursday.onValueChanged.AddListener(delegate { CheckWeekValidity(); });
		friday.onValueChanged.AddListener(delegate { CheckWeekValidity(); });
		saturday.onValueChanged.AddListener(delegate { CheckWeekValidity(); });
		sunday.onValueChanged.AddListener(delegate { CheckWeekValidity(); });

		//check to make sure this works!!!
		startYearInput.onValueChanged.AddListener(delegate {
			OnInputValueChanged(4, startYearInput.text,startYearInput,startMonthInput); });

		startMonthInput.onValueChanged.AddListener(delegate {
			OnInputValueChanged(2, startMonthInput.text, startMonthInput, startDayInput);
		});

		startDayInput.onValueChanged.AddListener(delegate {
			OnInputValueChanged(2, startDayInput.text, startDayInput, null);
		});

		endYearInput.onValueChanged.AddListener(delegate {
			OnInputValueChanged(4, endYearInput.text, endYearInput, endMonthInput);
		});

		endMonthInput.onValueChanged.AddListener(delegate {
			OnInputValueChanged(2, endMonthInput.text, endMonthInput, endDayInput);
		});

		endDayInput.onValueChanged.AddListener(delegate {
			OnInputValueChanged(2, endDayInput.text, endDayInput, null);
		});

		errorText.gameObject.SetActive(false);

		hasLength.onValueChanged.AddListener(HasLengthToggleChanged);
		lengthEntry.SetActive(false);

		InitializeRepititionType();
	}

	protected void OnInputValueChanged(int length, string text, InputField current, InputField next)
	{
		Debug.Log("received");
		if (text.Length == length && next != null)
		{
			current.DeactivateInputField();
			next.ActivateInputField();
		}
		else if (text.Length == length)
		{
			current.DeactivateInputField();
		}
	}

	protected void OnRepititionUnitChanged()
	{

	}

	protected void OnColorPickerButtonCliked()
	{
		//activate color picker
		colorBlocker.SetActive(true);
		colorPicker.gameObject.SetActive(true);
	}

	protected void OnColorPicked()
	{
		colorButton.image.color = colorPicker.TheColor;
		habitColor = colorPicker.TheColor;
		colorBlocker.SetActive(false);
		colorPicker.gameObject.SetActive(false);
	}

	private void ToggleChanged()
	{
		//Debug.Log("changing");
		if (onDay.isOn)
		{
			switch((RepititionType)repititionDropdown.value)
			{
				case RepititionType.SECONDLY:
				case RepititionType.MINUTELY:
				case RepititionType.HOURLY:
				case RepititionType.DAILY:
				case RepititionType.WEEKLY:
					//Debug.Log("jere");
					//months.gameObject.SetActive(false);
					//dayOfMonth.gameObject.SetActive(false);

					//ordinalDropdown.gameObject.SetActive(false);
					//month.gameObject.SetActive(false);
					//daysOfWeek.gameObject.SetActive(false);

					onDayHolder.SetActive(false);
					onTheHolder.SetActive(false);
					break;
				case RepititionType.YEARLY:

					months.gameObject.SetActive(true);
					dayOfMonth.gameObject.SetActive(true);

					dayOfMonth.maxValue = DateTime.DaysInMonth(DateTime.Now.Year, (int)Enum.Parse(typeof(Months), months.options[months.value].text));

					//onTheHolder.SetActive(false);
					ordinalDropdown.gameObject.SetActive(false);
					month.gameObject.SetActive(false);
					daysOfWeek.gameObject.SetActive(false);

					break;
				case RepititionType.MONTHLY:
					
					dayOfMonth.gameObject.SetActive(true);
					dayOfMonth.maxValue = 31;

					months.gameObject.SetActive(false);
					ordinalDropdown.gameObject.SetActive(false);
					month.gameObject.SetActive(false);
					daysOfWeek.gameObject.SetActive(false);
					break;
				default:
					break;
			}

		}
		else
		{
			switch ((RepititionType)repititionDropdown.value)
			{
				case RepititionType.SECONDLY:
				case RepititionType.MINUTELY:
				case RepititionType.HOURLY:
				case RepititionType.DAILY:
				case RepititionType.WEEKLY:
					//Debug.Log("here");
					//months.gameObject.SetActive(false);
					//dayOfMonth.gameObject.SetActive(false);

					//ordinalDropdown.gameObject.SetActive(false);
					//month.gameObject.SetActive(false);
					//daysOfWeek.gameObject.SetActive(false);

					onTheHolder.SetActive(false);
					onDayHolder.SetActive(false);
					break;
				case RepititionType.YEARLY:

					months.gameObject.SetActive(false);
					dayOfMonth.gameObject.SetActive(false);

					ordinalDropdown.gameObject.SetActive(true);

					

					month.gameObject.SetActive(true);
					daysOfWeek.gameObject.SetActive(true);

					break;
				case RepititionType.MONTHLY:
					months.gameObject.SetActive(false);
					dayOfMonth.gameObject.SetActive(false);

					ordinalDropdown.gameObject.SetActive(true);

					daysOfWeek.gameObject.SetActive(true);

					month.gameObject.SetActive(false);
					
					break;
				default:
					break;
			}
		}
	}

	//public void ShowPanel()
	//{
	//	panel.SetActive(true);
	//}

	private void HasLengthToggleChanged(bool hl)
	{
		lengthEntry.SetActive(hl);
	}

	bool CheckWeekValidity()
	{
		int selected = 0;
		if (monday.isOn)
		{
			selected++;
		}

		if (tuesday.isOn)
		{
			selected++;
		}

		if (wednesday.isOn)
		{
			selected++;
		}

		if (thursday.isOn)
		{
			selected++;
		}

		if (friday.isOn)
		{
			selected++;
		}

		if (saturday.isOn)
		{
			selected++;
		}

		if (sunday.isOn)
		{
			selected++;
		}

		if (selected < 1)
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "At least one day must be selected";
		}

		return selected >= 1;
	}

	void InitializeRepititionType()
	{
		repititionDropdown.ClearOptions();
		repititionDropdown.onValueChanged.AddListener(RepititionTypeChanged);
		List<string> options = new List<string>();
		foreach (var item in Enum.GetValues(typeof(RepititionType)))
		{
			options.Add(item.ToString());
		}

		repititionDropdown.AddOptions(options);

		repititionDropdown.value = 1;
		repititionDropdown.value = 0;
		ending.value = 1;
		ending.value = 0;
	}

	void RepititionTypeChanged(int x)
	{
		//Debug.Log(((RepititionType)x).ToString());

		switch ((RepititionType)x)
		{
			case RepititionType.SECONDLY:
				intervalPeriodName.text = "second (s)";
				break;
			case RepititionType.MINUTELY:
				intervalPeriodName.text = "minute (s)";
				break;
			case RepititionType.HOURLY:
				intervalPeriodName.text = "hour (s)";
				break;
			case RepititionType.DAILY:
				intervalPeriodName.text = "day (s)";
				break;
			case RepititionType.WEEKLY:
				intervalPeriodName.text = "week (s)";
				break;
			case RepititionType.MONTHLY:
				intervalPeriodName.text = "month (s)";
				break;
			case RepititionType.YEARLY:
				intervalPeriodName.text = "year (s)";
				break;
			default:
				break;
		}
		if ((RepititionType)x == RepititionType.WEEKLY)
		{
			weeklyHolder.SetActive(true);
		}
		else
		{
			weeklyHolder.SetActive(false);
		}

		switch ((RepititionType)x)
		{
			case RepititionType.SECONDLY:
			case RepititionType.MINUTELY:
			case RepititionType.HOURLY:
				timeEntryHolder.SetActive(false);
				break;
			case RepititionType.DAILY:
			case RepititionType.WEEKLY:
			case RepititionType.MONTHLY:
			case RepititionType.YEARLY:
				timeEntryHolder.SetActive(true);
				break;
			default:
				break;
		}

		SetToggles((RepititionType)x);
		SetOrdinal((RepititionType)x);
	}

	void EndingChanged(int x)
	{
		//Debug.Log("Changing ending");
		switch((EndType)x)
		{
			case EndType.NEVER:
				countHolder.gameObject.SetActive(false);
				endDateEntryHolder.SetActive(false);
				pickFromCalendarEndButton.onClick.RemoveAllListeners();
				break;
			case EndType.AFTER:
				endDateEntryHolder.SetActive(false);
				pickFromCalendarEndButton.onClick.RemoveAllListeners();
				countHolder.gameObject.SetActive(true);
				break;
			case EndType.ON:
				countHolder.gameObject.SetActive(false);
				endDateEntryHolder.SetActive(true);
				pickFromCalendarEndButton.onClick.AddListener(OnCalendarEndClicked);
				break;
		}
	}

	bool isValidTime()
	{
		int hour, minute;
		Int32.TryParse(timeHourInput.text, out hour);
		if (hour >= 0 && hour < 24)
		{
			Int32.TryParse(timeMinuteInput.text, out minute);
			if (minute >= 0 && minute < 60)
			{
				return true;
			}
		}

		return false;
	}

	bool IsValidEndDate()
	{
		int year, month, day;
		Int32.TryParse(endYearInput.text, out year);
		if (year >= 0)
		{
			Int32.TryParse(endMonthInput.text, out month);
			if (month >= 1 && month <= 12 )
			{
				Int32.TryParse(endDayInput.text, out day);
				if (day >= 1 && day <= DateTime.DaysInMonth(year,month))
				{
					return true;
				}
			}
		}

		return false;
	}

	bool IsValidStartDate()
	{
		int year, month, day;
		Int32.TryParse(startYearInput.text, out year);
		if (year >= 0)
		{
			Int32.TryParse(startMonthInput.text, out month);
			if (month >= 1 && month <= 12)
			{
				Int32.TryParse(startDayInput.text, out day);
				if (day >= 1 && day <= DateTime.DaysInMonth(year, month))
				{
					return true;
				}
			}
		}

		return false;
	}

	void OnOrdinalChanged(int x)
	{
		//Debug.Log("int: " + x);
		//Debug.Log("enum: " + (Enum.Parse(typeof(Ordinal),ordinalDropdown.options[x].text)).ToString());
	}

	bool IsValidCount()
	{
		int c;
		Int32.TryParse(count.text, out c);
		return c >= 1;
	}

	bool IsValidName()
	{
		return !habitName.text.Equals("");
	}

	void SetOrdinal(RepititionType rt)
	{
		switch (rt)
		{
			case RepititionType.SECONDLY:
			case RepititionType.MINUTELY:
			case RepititionType.HOURLY:
			case RepititionType.DAILY:
			case RepititionType.WEEKLY:
				ordinalDropdown.gameObject.SetActive(false);
				break;
			case RepititionType.YEARLY:
			case RepititionType.MONTHLY:

				//ordinalDropdown.gameObject.SetActive(true);
				onDay.transform.parent.gameObject.SetActive(true);
				onThe.transform.parent.gameObject.SetActive(true);
				onThe.gameObject.SetActive(true);
				onDay.gameObject.SetActive(true);
				break;
			default:
				break;
		}

	}

	void SetToggles(RepititionType rt)
	{
		//Debug.Log("setting toggles");
		switch (rt)
		{
			case RepititionType.SECONDLY:
			case RepititionType.MINUTELY:
			case RepititionType.HOURLY:
			case RepititionType.DAILY:
			case RepititionType.WEEKLY:
				//Debug.Log("not visible");
				//onDay.transform.parent.gameObject.SetActive(false);
				//onThe.transform.parent.gameObject.SetActive(false);
				//onThe.gameObject.SetActive(false);
				//onDay.gameObject.SetActive(false);

				//months.gameObject.SetActive(false);
				//dayOfMonth.gameObject.SetActive(false);

				//ordinalDropdown.gameObject.SetActive(false);
				//month.gameObject.SetActive(false);
				//daysOfWeek.gameObject.SetActive(false);

				onDayHolder.SetActive(false);
				onTheHolder.SetActive(false);
				break;
			case RepititionType.YEARLY:
			case RepititionType.MONTHLY:
				//Debug.Log("visible");
				//onThe.gameObject.SetActive(true);
				onDayHolder.SetActive(true);
				onTheHolder.SetActive(true);
				onDay.isOn = false;
				onDay.isOn = true;
				//onDay.gameObject.SetActive(true);
				break;
			default:
				break;
		}
	}

	public void CreateHabit()
	{
		if(!IsValidName())
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "Please enter a name";
			return;
		}

		if (!IsValidStartDate())
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "Start date is not valid";
			return;
		}

		if ((EndType)ending.value == EndType.ON && !IsValidEndDate())
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "End date is not valid";
			return;
		}
		else if ((EndType)ending.value == EndType.AFTER && !IsValidCount())
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "Count must be positive";
			return;
		}

		int i = Int32.Parse(interval.text);
		if (i <= 0)
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "Interval must be positive abd greater than zero";
			return;
		}

		if (habitColor.Equals(new Color(0f,0f,0f,0f)))
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "Pick a color";
			return;
		}

		errorText.gameObject.SetActive(false);

		var pattern = new RecurrencePattern();
		//{
		//	Frequency = FrequencyType.Weekly,
		//	ByDay = new List<IWeekDay> { new WeekDay(DayOfWeek.Friday) },
		//	ByMonth = new List<int> { 1}

		//};

		switch ((RepititionType)repititionDropdown.value)
		{
			case RepititionType.SECONDLY:
				pattern.Frequency = FrequencyType.Secondly;
				break;
			case RepititionType.MINUTELY:
				pattern.Frequency = FrequencyType.Minutely;
				break;
			case RepititionType.HOURLY:
				pattern.Frequency = FrequencyType.Hourly;
				break;
			case RepititionType.DAILY:
				pattern.Frequency = FrequencyType.Daily;
				break;
			case RepititionType.WEEKLY:
				pattern.Frequency = FrequencyType.Weekly;

				pattern.ByDay = new List<IWeekDay>();
				if (monday.isOn)
				{
					pattern.ByDay.Add(new WeekDay(DayOfWeek.Monday));
				}

				if (tuesday.isOn)
				{
					pattern.ByDay.Add(new WeekDay(DayOfWeek.Tuesday));
				}

				if (wednesday.isOn)
				{
					pattern.ByDay.Add(new WeekDay(DayOfWeek.Wednesday));
				}

				if (thursday.isOn)
				{
					pattern.ByDay.Add(new WeekDay(DayOfWeek.Thursday));
				}

				if (friday.isOn)
				{
					pattern.ByDay.Add(new WeekDay(DayOfWeek.Friday));
				}

				if (saturday.isOn)
				{
					pattern.ByDay.Add(new WeekDay(DayOfWeek.Saturday));
				}

				if (sunday.isOn)
				{
					pattern.ByDay.Add(new WeekDay(DayOfWeek.Sunday));
				}


				break;
			case RepititionType.MONTHLY:
				pattern.Frequency = FrequencyType.Monthly;
				if (onDay.isOn)
				{
					pattern.ByMonthDay = new List<int>() { (int)dayOfMonth.value};
				}
				else
				{
					pattern.BySetPosition = new List<int>() { (int)(Enum.Parse(typeof(Ordinal),
						ordinalDropdown.options[ordinalDropdown.value].text)) };

					pattern.ByDay = new List<IWeekDay>() {new WeekDay((DayOfWeek)daysOfWeek.value) };
				}
				break;
			case RepititionType.YEARLY:
				pattern.Frequency = FrequencyType.Yearly;
				if (onDay.isOn)
				{
					pattern.ByMonth = new List<int>() {months.value+1 };
					pattern.ByMonthDay = new List<int>() { (int)dayOfMonth.value };
				}
				else
				{
					pattern.ByMonth = new List<int>() { month.value + 1 };

					pattern.BySetPosition = new List<int>() { (int)(Enum.Parse(typeof(Ordinal),
						ordinalDropdown.options[ordinalDropdown.value].text)) };

					pattern.ByDay = new List<IWeekDay>() { new WeekDay((DayOfWeek)daysOfWeek.value) };
				}
				break;
			default:
				break;
		}

		switch ((RepititionType)repititionDropdown.value)
		{
			case RepititionType.SECONDLY:
			case RepititionType.MINUTELY:
			case RepititionType.HOURLY:
				//we don't care about the time entry
				break;
			case RepititionType.DAILY:
			case RepititionType.WEEKLY:
			case RepititionType.MONTHLY:
			case RepititionType.YEARLY:
				//we need to set the time entry
				pattern.ByHour = new List<int>() { Int32.Parse(timeHourInput.text) };
				pattern.ByMinute = new List<int>() { Int32.Parse(timeMinuteInput.text) };
				break;
			default:
				break;
		}

		pattern.Interval = Int32.Parse(interval.text);

		if ((EndType)ending.value == EndType.AFTER)
		{
			pattern.Count = Int32.Parse(count.text);
		}
		else if ((EndType)ending.value == EndType.ON)
		{
			//we should do sanity checks here
			pattern.Until = new DateTime(Int32.Parse(endYearInput.text), Int32.Parse(endMonthInput.text), Int32.Parse(endDayInput.text));
		}
		//pattern.u

		

		RecurrencePatternSerializer serializer = new RecurrencePatternSerializer();

		string pat = serializer.SerializeToString(pattern);
		DateTime start = new DateTime(Int32.Parse(startYearInput.text), Int32.Parse(startMonthInput.text), Int32.Parse(startDayInput.text));


		RepUnit ru = new RepUnit();
		if (hasLength.isOn)
		{
			if (!string.IsNullOrEmpty(length.text))
			{

				ru.numberOfReps = Int32.Parse(length.text);
				ru.unitType = (RepititionUnitType)unitName.value;
			}
		}
		
		

		Habit hab = new Habit(habitName.text,new List<string>() { pat}, start, ru, habitColor);

		if (HabitCreated != null)
		{
			HabitCreated(hab);
		}

		//FileStream fileStream;
		//BinaryFormatter bf = new BinaryFormatter();
		//List<Habit> habits;
		//if (File.Exists(Application.persistentDataPath + "Habits.dat"))
		//{
		//	fileStream = new FileStream(Application.persistentDataPath + "Habits.dat", FileMode.Open);
		//	habits = bf.Deserialize(fileStream) as List<Habit>;
		//	fileStream.Close();
		//}
		//else
		//{
		//	habits = new List<Habit>();
		//}

		

		//habits.Add(hab);

		//fileStream = new FileStream(Application.persistentDataPath + "Habits.dat", FileMode.OpenOrCreate);
		////FileStream fileStream = new FileStream(Application.persistentDataPath+"Pattern.dat", FileMode.Create);
		////serializer.Serialize(pattern, fileStream, System.Text.Encoding.UTF8);
		////bf = new BinaryFormatter();
		//bf.Serialize(fileStream, habits);
		//fileStream.Close();

		////this seems to be working
		//RecurrencePatternEvaluator re = new RecurrencePatternEvaluator(pattern);

		//Debug.Log(pattern.ToString());

		//HashSet<IPeriod> p = re.Evaluate(new CalDateTime(new DateTime(2019, 12, 1)), new DateTime(2019,12,1), new DateTime(2020,1,11), false);

		//Debug.Log(p.Count);

		////HashSet<DateTime> dt = re.GetDates(new CalDateTime(DateTime.Today), new DateTime(2020, 1, 9), new DateTime(2020, 1, 11), 100, pattern, true);

		//foreach (var item in p)
		//{
		//	Debug.Log(item.StartTime);
		//	Debug.Log(item.EndTime);
		//}

		////fileStream.r
		//fileStream = new FileStream(Application.persistentDataPath + "Habits.dat", FileMode.Open);
		//var h = bf.Deserialize(fileStream);
		//var hs = h as List<Habit>;
		//foreach (var ha in hs)
		//{
		//	Debug.Log(ha.HabitName);
		//	foreach (var item in ha.RepititionRules)
		//	{
		//		Debug.Log(item);
		//	}
		//}
		////var newPattern = serializer.Deserialize(fileStream, System.Text.Encoding.UTF8);
		

		//fileStream.Close();

		//Debug.Log(newPattern.ToString());
	}

	public void OnDisable()
	{
		((Text)habitName.placeholder).text = "Enter text";
		habitName.text = "";

		((Text)startYearInput.placeholder).text = "Enter text";
		startYearInput.text = "";

		((Text)startMonthInput.placeholder).text = "Enter text";
		startMonthInput.text = "";

		((Text)startDayInput.placeholder).text = "Enter text";
		startDayInput.text = "";

		((Text)endYearInput.placeholder).text = "Enter text";
		endYearInput.text = "";

		((Text)endMonthInput.placeholder).text = "Enter text";
		endMonthInput.text = "";

		((Text)endDayInput.placeholder).text = "Enter text";
		endDayInput.text = "";

		((Text)timeHourInput.placeholder).text = "Enter text";
		timeHourInput.text = "";

		((Text)timeMinuteInput.placeholder).text = "Enter text";
		timeMinuteInput.text = "";

		((Text)count.placeholder).text = "Enter text";
		count.text = "";

		((Text)length.placeholder).text = "Enter text";
		length.text = "";

		ending.value = 0;
		repititionDropdown.value = 0;

		onDay.isOn = true;
		months.value = 0;
		dayOfMonth.value = 1;

		ordinalDropdown.value = 0;
		daysOfWeek.value = 0;
		month.value = 0;

		monday.isOn = true;
		tuesday.isOn = false;
		wednesday.isOn = false;
		thursday.isOn = false;
		friday.isOn = false;
		saturday.isOn = false;
		sunday.isOn = false;

		unitName.value = 0;

		((Text)interval.placeholder).text = "Enter text";
		interval.text = "1";

		errorText.text = "";
		errorText.gameObject.SetActive(false);

		colorButton.onClick.RemoveAllListeners();
		acceptColorButton.onClick.RemoveAllListeners();

		colorPicker.TheColor = new Color(0f, 0f, 0f, 0f);
	}
}
