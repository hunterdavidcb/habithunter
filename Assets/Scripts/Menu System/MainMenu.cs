﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

using Ical.Net.DataTypes;
using Ical.Net.Evaluation;
using Ical.Net.Interfaces.DataTypes;

using Unity.Notifications.Android;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

	public GameObject mainPanel;
	public GameObject createHabitPanel;
	public GameObject calendarPanel;
	public GameObject allHabitViewerPanel;
	public GameObject habitViewPanel;
	public GameObject checkTodayPanel;
	public GameObject viewChoicePanel;

	public GameObject createButton;
	public GameObject viewButton;

	public AudioClip click;
	public AudioClip achieve;

	Dictionary<string, GameObject> panelDictionary = new Dictionary<string, GameObject>();
	Dictionary<string, GameObject> panelNameToButtonDictionary = new Dictionary<string, GameObject>();

	static readonly string main = "MainPanel";
	GameObject currentPanel;
	//Stack<string> previousPanels = new Stack<string>();

	public static MainMenu instance;

	Dictionary<string,Habit> habits;
	Dictionary<int, string> notificationHabitDictionary = new Dictionary<int, string>();
	FileStream fileStream;
	BinaryFormatter bf;

	public delegate void ViewHabitHandler(Habit habit);
	public event ViewHabitHandler ViewPreviousClicked;
	public event ViewHabitHandler ViewNextClicked;

	AudioSource source;
	Animator mainButtonHolderAnim;

	// Start is called before the first frame update
	void Start()
	{
		sqrDeadzone = deadZone * deadZone;

		mainButtonHolderAnim = mainPanel.transform.GetChild(0).GetComponent<Animator>(); 
		source = GetComponent<AudioSource>();

		//get the data
		DeserializeHabits();
		DeserializeNotifications();

		// setup notifications
		var androidNotificationChannel = new AndroidNotificationChannel()
		{
			Id = "Habit_Hunter_Channel",
			Name = "Habit Hunter",
			Importance = Importance.High,
			Description = "Notifications for habits"
		};

		if (!AndroidNotificationCenter.GetNotificationChannels().Contains(androidNotificationChannel))
		{
			Debug.Log("registering");
			AndroidNotificationCenter.RegisterNotificationChannel(androidNotificationChannel);
		}


		var notification = new AndroidNotification()
		{
			Title = "Habit Hunter",
			Text = "Remember to check your habits",
			FireTime = DateTime.Today.AddHours(23),
			SmallIcon = "habit",
			RepeatInterval = new TimeSpan(1,0,0,0)
		};

		//AndroidNotificationCenter.UpdateScheduledNotification(1, notification, androidNotificationChannel.Id);

		//if (AndroidNotificationCenter.CheckScheduledNotificationStatus(1) == NotificationStatus.Unavailable ||
		//	AndroidNotificationCenter.CheckScheduledNotificationStatus(1) == NotificationStatus.Unknown)
		//{

		AndroidNotificationCenter.SendNotificationWithExplicitID(notification, androidNotificationChannel.Id, 1);
		if (!notificationHabitDictionary.ContainsKey(1))
		{
			notificationHabitDictionary.Add(1, "Habit Hunter");
			SerializeNotifications();
		}

		var hollow = new AndroidNotification()
		{
			Title = "Habit Hunter",
			Text = "Processing habits",
			FireTime = DateTime.Today.AddDays(1).AddHours(1),
			SmallIcon = "habit",
			RepeatInterval = new TimeSpan(1, 0, 0, 0)
		};

		AndroidNotificationCenter.SendNotificationWithExplicitID(hollow, androidNotificationChannel.Id, 2);
		AndroidNotificationCenter.OnNotificationReceived += ProcessHabits;
		if (!notificationHabitDictionary.ContainsKey(2))
		{
			notificationHabitDictionary.Add(2, "Processing habits");
			SerializeNotifications();
		}

		//}

		ScheduleNotifications(androidNotificationChannel);

		instance = this;


		//currentPanel = main;
		currentPanel = mainPanel;

		//panelDictionary.Add(currentPanel, mainPanel);
		panelDictionary.Add(createHabitPanel.name, createHabitPanel);
		panelDictionary.Add(calendarPanel.name, calendarPanel);
		panelDictionary.Add(allHabitViewerPanel.name, allHabitViewerPanel);
		panelDictionary.Add(checkTodayPanel.name, checkTodayPanel);
		panelDictionary.Add(viewChoicePanel.name, viewChoicePanel);
		panelDictionary.Add(habitViewPanel.name, habitViewPanel);

		panelDictionary.Add(calendarPanel.GetComponent<CalendarPanel>().dailyPanel.name,
			calendarPanel.GetComponent<CalendarPanel>().dailyPanel);

		panelDictionary.Add(calendarPanel.GetComponent<CalendarPanel>().weeklyPanel.name,
			calendarPanel.GetComponent<CalendarPanel>().weeklyPanel);

		panelDictionary.Add(calendarPanel.GetComponent<CalendarPanel>().monthlyPanel.name,
			calendarPanel.GetComponent<CalendarPanel>().monthlyPanel);


		panelNameToButtonDictionary.Add(createHabitPanel.name, createButton);
		//panelNameToButtonDictionary.Add(calenderPanel.name, cal);
		panelNameToButtonDictionary.Add(allHabitViewerPanel.name, 
			viewChoicePanel.GetComponent<ViewChoicePanel>().viewAllButton.gameObject);

		panelNameToButtonDictionary.Add(calendarPanel.GetComponent<CalendarPanel>().dailyPanel.name, 
			viewChoicePanel.GetComponent<ViewChoicePanel>().viewDayButton.gameObject);

		panelNameToButtonDictionary.Add(calendarPanel.GetComponent<CalendarPanel>().weeklyPanel.name,
			viewChoicePanel.GetComponent<ViewChoicePanel>().viewWeekButton.gameObject);

		panelNameToButtonDictionary.Add(calendarPanel.GetComponent<CalendarPanel>().monthlyPanel.name,
			viewChoicePanel.GetComponent<ViewChoicePanel>().viewMonthButton.gameObject);

		panelNameToButtonDictionary.Add(checkTodayPanel.name,
			viewChoicePanel.GetComponent<ViewChoicePanel>().viewTodayButton.gameObject);

		//panelNameToButtonDictionary.Add(checkTodayPanel.name, createButton);
		panelNameToButtonDictionary.Add(viewChoicePanel.name, viewButton);

		//set up main panel button listeners
		createButton.GetComponent<Button>().onClick.AddListener(OnCreateButtonPressed);
		viewButton.GetComponent<Button>().onClick.AddListener(OnChangeViewButtonPressed);

		//createButton.GetComponent<HitTest>().ClickFinished += OnClickFinished;
		//viewButton.GetComponent<HitTest>().ClickFinished += OnClickFinished;

		//set up view choice panel listeners
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewAllButton.onClick.AddListener(OnViewAllButtonPressed);
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewDayButton.onClick.AddListener(OnViewDayButtonPressed);
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewMonthButton.onClick.AddListener(OnViewMonthButtonPressed);
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewTodayButton.onClick.AddListener(OnViewTodayButtonPressed);
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewWeekButton.onClick.AddListener(OnViewWeekButtonPressed);

		//set up delegates
		createHabitPanel.GetComponent<CreateHabitMenu>().HabitCreated += OnHabitCreated;
		checkTodayPanel.GetComponent<CheckTodayPanel>().CheckFinished += OnCheckFinished;
		calendarPanel.GetComponent<CalendarPanel>().dailyPanel.GetComponent<DayPanel>().HabitClicked += OnViewHabitDetails;
		calendarPanel.GetComponent<CalendarPanel>().monthlyPanel.GetComponent<MonthPanel>().MonthDayClicked += OnMonthDayButtonPressed;
		calendarPanel.GetComponent<CalendarPanel>().weeklyPanel.GetComponent<WeekPanel>().DayClicked += OnWeekDayClicked;
		calendarPanel.GetComponent<CalendarPanel>().weeklyPanel.GetComponent<WeekPanel>().HabitClicked += OnViewHabitDetails;
		//foreach (var habit in habits)
		//{
		//	Debug.Log(habit.Value.HabitName);
		//	//RecurrencePatternSerializer serializer = new RecurrencePatternSerializer();
		//	//var newPattern = serializer.Deserialize(fileStream, System.Text.Encoding.UTF8);
		//	//RecurrencePatternEvaluator re = new RecurrencePatternEvaluator(newPattern);
		//	for (int i = 0; i < habit.Value.RepititionRules.Count; i++)
		//	{
		//		var pa = new RecurrencePattern(habit.Value.RepititionRules[i]);
		//		Debug.Log(pa.ToString());
		//		RecurrencePatternEvaluator re = new RecurrencePatternEvaluator(pa);
		//		//Debug.Log(pa.ByHour[0]);
		//		//Debug.Log(pa.ByMinute[0]);
		//		//Debug.Log(DateTime.Now.Hour);
		//		//Debug.Log((pa.ByHour[0] - DateTime.Now.Hour));
		//		DateTime next = re.GetNextDate(new CalDateTime(DateTime.Today.AddSeconds(1)),
		//			DateTime.Now.AddDays(3));
		//		Debug.Log(next.ToString());
		//		//Debug.Log(habit.Value.Start);
		//		//HashSet<IPeriod> periods = re.Evaluate(new CalDateTime(DateTime.Today.AddSeconds(1)), habit.Value.Start,
		//		//	DateTime.Today.AddDays(2).AddSeconds(1), false);
		//		//Debug.Log(periods.Count);
		//		//foreach (var period in periods)
		//		//{
		//		//	if (period.StartTime.HasTime)
		//		//	{
		//		//		Debug.Log(period.StartTime.ToString() + " at " + period.StartTime.Hour + ":" + period.StartTime.Minute);
		//		//	}

		//		////	todaysHabits.Add(habit.Value);
		//		//}
		//	}

		//}


	}

	private void Update()
	{
		//detect back button / escape button presses
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			//react accordingly
			OnBackButtonPressed();
		}

		if (swipeLeft)
		{
			mainButtonHolderAnim.SetTrigger("SwipeLeft");
		}

		if (swipeRight)
		{
			mainButtonHolderAnim.SetTrigger("SwipeRight");
		}

		tap = doubleTap = swipeDown = swipeLeft = swipeRight = swipeUp = false;

#if UNITY_EDITOR
		DetectSwipesStandalone();
#else
		DetectSwipesMobile();
#endif
	}

	//private void OnClickFinished(GameObject self)
	//{
	//	//if (self.name.Equals(createButton.name))
	//	//{
	//	//	self.SetActive(false);
	//	//}

	//	//if (self.name.Equals(viewButton.name))
	//	//{
	//	//	self.SetActive(false);
	//	//}

	//	//if (self.name.Equals(backButton.name))
	//	//{
	//	//	if (previousPanels.Count == 0)
	//	//	{
	//	//		backButton.SetActive(false);
	//	//	}
	//	//	else
	//	//	{
	//	//		backButton.SetActive(true);
	//	//	}
	//	//}
	//}

	protected void OnWeekDayClicked(DateTime dateTime)
	{
		panelDictionary[currentPanel.name].SetActive(false);

		panelDictionary[calendarPanel.GetComponent<CalendarPanel>().dailyPanel.name].SetActive(true);

		//previousPanels.Push(currentPanel.name);

		currentPanel = calendarPanel.GetComponent<CalendarPanel>().dailyPanel;

		DayPanel dp = panelDictionary[calendarPanel.GetComponent<CalendarPanel>().dailyPanel.name].GetComponent<DayPanel>();

		dp.dayInfo.text = dateTime.DayOfWeek.ToString() + "\n (" +
			((Months)dateTime.Month).ToString() + " " + dateTime.Day + ", " + dateTime.Year + ")";

		dp.previousDayButton.onClick.RemoveAllListeners();
		dp.nextDayButton.onClick.RemoveAllListeners();

		dp.previousDayButton.onClick.AddListener(delegate
		{ OnPreviousDayButtonPressed(dateTime); });
		dp.nextDayButton.onClick.AddListener(delegate
		{ OnNextDayButtonPressed(dateTime); });

		//update the habits
		Dictionary<Habit, List<IPeriod>> thisDaysHabits = GetHabitsBetween(dateTime, dateTime.AddDays(1)); //new Dictionary<Habit, List<IPeriod>>();

		dp.SpawnHabits(thisDaysHabits);
	}

	public void PlayButton()
	{
		source.clip = click;
		source.Play();
	}

#region Notifications


	private void ScheduleNotifications(AndroidNotificationChannel channel)
	{
		int id = 3;
		foreach (var habit in habits.Values)
		{
			var pa = new RecurrencePattern(habit.RepititionRules[0]);
			RecurrencePatternEvaluator re = new RecurrencePatternEvaluator(pa);
			DateTime next = re.GetNextDate(new CalDateTime(DateTime.Today.AddSeconds(1)),
						DateTime.Now);

			var notification = new AndroidNotification()
			{
				Title = "Habit Hunter",
				Text = "Did you " + habit.HabitName + " today?",
				//FireTime = DateTime.Now.AddSeconds(id),
				FireTime = next,
				SmallIcon = "habit",
				
				//RepeatInterval = new TimeSpan(1, 0, 0, 0)
			};

			if (pa.Frequency == Ical.Net.FrequencyType.Daily)
			{
				notification.RepeatInterval = new TimeSpan(pa.Interval, 0, 0, 0);
			}
			else if (pa.Frequency == Ical.Net.FrequencyType.Hourly)
			{
				notification.RepeatInterval = new TimeSpan(0, pa.Interval, 0, 0);
			}
			else if (pa.Frequency == Ical.Net.FrequencyType.Minutely)
			{
				notification.RepeatInterval = new TimeSpan(0, 0, pa.Interval, 0);
			}
			else if (pa.Frequency == Ical.Net.FrequencyType.Secondly)
			{
				notification.RepeatInterval = new TimeSpan(0, 0, 0, pa.Interval);
			}

			AndroidNotificationCenter.SendNotificationWithExplicitID(notification, channel.Id, id);

			AndroidNotificationCenter.OnNotificationReceived += OnNotificationSent;
			if (!notificationHabitDictionary.ContainsKey(id))
			{
				notificationHabitDictionary.Add(id, habit.HabitID);
				SerializeNotifications();
			}

			id++;

		}
	}

	protected void ProcessHabits(AndroidNotificationIntentData data)
	{
		if (data.Id == 2)
		{
			Dictionary<Habit, List<IPeriod>> habs = GetHabitsBetween(DateTime.Today.AddDays(-1),
				DateTime.Today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(58));
			foreach (var habit in habs.Keys)
			{
				foreach (var time in habs[habit])
				{
					//the user did not check this particular habit today, so fail them
					if (!habit.CompletedInstances.ContainsKey(time.StartTime.Value))
					{
						habit.CompletedInstances.Add(time.StartTime.Value, false);
					}
				}
			}
			//make sure to save the habits now that we've updated them
			SerializeHabits();
		}
	}

	protected void OnNotificationSent(AndroidNotificationIntentData data)
	{
		//data.Id
		//data.Channel
		//data.Notification.Text
		//data.Notification.Title
		//data.Notification.FireTime
		//Debug.Log("here");
	}


#endregion

#region Serialization

	private void SerializeHabits()
	{
		fileStream = new FileStream(Application.persistentDataPath + "/Habits.dat", FileMode.OpenOrCreate);
		bf = new BinaryFormatter();
		bf.Serialize(fileStream, habits);
		fileStream.Close();
	}

	private void DeserializeHabits()
	{
		bf = new BinaryFormatter();
		if (File.Exists(Application.persistentDataPath + "/Habits.dat"))
		{
			fileStream = new FileStream(Application.persistentDataPath + "/Habits.dat", FileMode.Open);
			habits = bf.Deserialize(fileStream) as Dictionary<string, Habit>;
			fileStream.Close();
		}
		else
		{
			habits = new Dictionary<string, Habit>();
		}
	}

	private void SerializeNotifications()
	{
		fileStream = new FileStream(Application.persistentDataPath + "/Notifications.dat", FileMode.OpenOrCreate);
		bf = new BinaryFormatter();
		bf.Serialize(fileStream, notificationHabitDictionary);
		fileStream.Close();
	}

	private void DeserializeNotifications()
	{
		bf = new BinaryFormatter();
		if (File.Exists(Application.persistentDataPath + "/Notifications.dat"))
		{
			fileStream = new FileStream(Application.persistentDataPath + "/Notifications.dat", FileMode.Open);
			notificationHabitDictionary = bf.Deserialize(fileStream) as Dictionary<int, string>;
			fileStream.Close();
		}
		else
		{
			notificationHabitDictionary = new Dictionary<int, string>();
		}
	}

	protected void OnHabitCreated(Habit hab)
	{
		habits.Add(hab.HabitID,hab);
		SerializeHabits();

		//DEBUGING code
		//foreach (var habit in habits)
		//{
		//	Debug.Log(habit.Value.HabitName);
		//	foreach (var item in habit.Value.RepititionRules)
		//	{
		//		Debug.Log(item);
		//	}
		//}

		createHabitPanel.SetActive(false);
		createButton.SetActive(true);
		//currentPanel = previousPanels.Pop();

	}

	public void UpdateHabit(Habit habit)
	{
		habits[habit.HabitID] = habit;
		SerializeHabits();
	}

#endregion

#region Main Buttons
	void OnCreateButtonPressed()
	{
		//createButton.SetActive(false);
		createButton.GetComponent<Button>().interactable = false;
		viewButton.SetActive(true);

		viewButton.GetComponent<Button>().interactable = true;

		//Debug.Log(currentPanel);

		foreach (var item in panelDictionary)
		{
			if (!item.Value.tag.Equals(main) && !item.Key.Equals(createHabitPanel.name))
			{
				//Debug.Log(item.Value.name);
				item.Value.SetActive(false);
			}

			if (item.Key == currentPanel.name)
			{
				//Debug.Log(item.Key);
				//previousPanels.Push(item.Key);
				if (panelNameToButtonDictionary.ContainsKey(item.Key))
				{
					panelNameToButtonDictionary[item.Key].SetActive(true);
				}
			}

			if (item.Key.Equals(createHabitPanel.name))
			{
				//Debug.Log(item.Key);
				item.Value.SetActive(true);
				//Debug.Log("here");
			}
		}

		//we set this at the end to take care of differences in access order in the dictionary
		currentPanel = createHabitPanel;


	}

	void OnChangeViewButtonPressed()
	{
		//viewButton.SetActive(false);

		viewButton.GetComponent<Button>().interactable = false;
		createButton.GetComponent<Button>().interactable = true;

		foreach (var item in panelDictionary)
		{
			if (!item.Value.tag.Equals(main) && item.Key != viewChoicePanel.name)
			{
				//Debug.Log(item.Value.name);
				item.Value.SetActive(false);
			}

			if (currentPanel != null)
			{
				if (item.Key == currentPanel.name)
				{
					//Debug.Log(item.Key);
					//previousPanels.Push(item.Key);
					if (panelNameToButtonDictionary.ContainsKey(item.Key))
					{
						panelNameToButtonDictionary[item.Key].SetActive(true);
					}
				}
			}
			

			if (item.Key == viewChoicePanel.name)
			{
				//Debug.Log(item.Key);
				item.Value.SetActive(true);
			}
		}



		//we set this at the end to take care of differences in access order in the dictionary
		currentPanel = viewChoicePanel;
	}

	void OnViewTodayButtonPressed()
	{
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewAllButton.interactable = true;
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewDayButton.interactable = true;
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewMonthButton.interactable = true;
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewTodayButton.interactable = false;
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewWeekButton.interactable = true;


		//previousPanels.Push(currentPanel);

		currentPanel = checkTodayPanel;

		checkTodayPanel.SetActive(true);


		Dictionary<Habit, List<IPeriod>> todaysHabits = GetHabitsBetween(DateTime.Today, DateTime.Today.AddDays(1)); //new Dictionary<Habit, List<IPeriod>>();

		checkTodayPanel.GetComponent<CheckTodayPanel>().SpawnHabits(todaysHabits);


		viewChoicePanel.SetActive(false);
	}

	void OnBackButtonPressed()
	{
		if (currentPanel == null)
		{
			Application.Quit();
		}
		else if (currentPanel.name.Contains("ly"))
		{
			currentPanel.SetActive(false);
			calendarPanel.gameObject.SetActive(false);
			currentPanel = null;
		}
		else
		{
			currentPanel.SetActive(false);
			currentPanel = null;
		}

		if (!createButton.GetComponent<Button>().interactable)
		{
			createButton.GetComponent<Button>().interactable = true;
		}

		if (!viewButton.GetComponent<Button>().interactable)
		{
			viewButton.GetComponent<Button>().interactable = true;
		}
		//bool found = false;
		//switch (currentPanel)
		//{
		//	case "MainPanel":
		//		break;
		//	case "CreateHabitPanel":
		//		createHabitPanel.SetActive(false);
		//		panelDictionary[previousPanels.Peek()].SetActive(true);
		//		currentPanel = previousPanels.Pop();

		//		//previousPanels.Count == 0?  : 
		//		//if (previousPanels.Count == 0)
		//		//{
		//		//	backButton.SetActive(false);
		//		//}
		//		//else
		//		//{
		//		//	backButton.SetActive(true);
		//		//}

		//		createButton.SetActive(true);

		//		if (panelNameToButtonDictionary.ContainsKey(currentPanel))
		//		{
		//			panelNameToButtonDictionary[currentPanel].SetActive(false);
		//		}

		//		found = true;

		//		break;
		//	case "CalendarPanel":

		//		panelDictionary[previousPanels.Peek()].SetActive(true);
		//		currentPanel = previousPanels.Pop();

		//		found = true;
		//		break;
		//	case "CheckTodayPanel":
		//		//Debug.Log("correct");
		//		checkTodayPanel.GetComponent<CheckTodayPanel>().ClearList();
		//		panelDictionary[previousPanels.Peek()].SetActive(true);
		//		checkTodayPanel.SetActive(false);

		//		currentPanel = previousPanels.Pop();

		//		if (panelNameToButtonDictionary.ContainsKey(currentPanel))
		//		{
		//			panelNameToButtonDictionary[currentPanel].SetActive(false);
		//		}

		//		//if (previousPanels.Count == 0)
		//		//{
		//		//	backButton.SetActive(false);
		//		//}
		//		//else
		//		//{
		//		//	backButton.SetActive(true);
		//		//}

		//		found = true;
		//		break;
		//	case "ViewChoicePanel":
		//		viewChoicePanel.SetActive(false);
		//		panelDictionary[previousPanels.Peek()].SetActive(true);
		//		currentPanel = previousPanels.Pop();

		//		//if (previousPanels.Count == 0)
		//		//{
		//		//	backButton.SetActive(false);
		//		//}
		//		//else
		//		//{
		//		//	backButton.SetActive(true);
		//		//}

		//		viewButton.SetActive(true);

		//		if (panelNameToButtonDictionary.ContainsKey(currentPanel))
		//		{
		//			panelNameToButtonDictionary[currentPanel].SetActive(false);
		//		}

		//		found = true;
		//		break;
		//	case "HabitViewerPanel":

		//		panelDictionary[previousPanels.Peek()].SetActive(true);

		//		panelDictionary[currentPanel].SetActive(false);

		//		currentPanel = previousPanels.Pop();

		//		found = true;
		//		break;
		//	default:
		//		break;
		//}

		//if (!found)
		//{
		//	//Debug.Log("not found");
		//	foreach (var item in panelDictionary)
		//	{
		//		if (item.Key == currentPanel)
		//		{
		//			//Debug.Log("deactivating");
		//			item.Value.SetActive(false);

		//			//we are probably dealing with a calender view
		//			if (item.Key.Contains("ly") && !previousPanels.Peek().Contains("ly"))
		//			{
		//				//Debug.Log("here");
		//				calendarPanel.SetActive(false);
		//			}

		//		}

		//		if (item.Key == previousPanels.Peek())
		//		{
		//			item.Value.SetActive(true);
		//			//there might be exceptions
		//			//if (panelNameToButtonDictionary.ContainsKey(item.Key))
		//			//{
		//			//	panelNameToButtonDictionary[item.Key].SetActive(false);
		//			//}
		//		}
		//	}

		//	//Debug.Log(currentPanel);

		//	currentPanel = previousPanels.Pop();

		//	//Debug.Log(currentPanel);

		//	//if (panelNameToButtonDictionary.ContainsKey(currentPanel))
		//	//{
		//	//	panelNameToButtonDictionary[currentPanel].SetActive(false);
		//	//	Debug.Log(panelNameToButtonDictionary[currentPanel].name);
		//	//}

		//	//if (previousPanels.Count == 0)
		//	//{
		//	//	backButton.SetActive(false);
		//	//}
		//	//else
		//	//{
		//	//	backButton.SetActive(true);
		//	//}
		//}
	}

	void OnQuitButtonPressed()
	{
		//make sure to save before this
		Application.Quit();
	}


#endregion

	protected void OnCheckFinished()
	{
		checkTodayPanel.SetActive(false);
		//currentPanel = previousPanels.Pop();

		viewButton.SetActive(true);
	}

	public void RegisterHabitFromViewAll(string habit, Button view, Button remove)
	{
		view.onClick.AddListener(delegate { OnViewHabitDetails(habit); });
		remove.onClick.AddListener(delegate { OnHabitRemoved(habit); });
	}

#region Viewing Habits

	protected void OnViewHabitDetails(string id)
	{
		//Debug.Log("heyheyeheyey");
		//previousPanels.Push(currentPanel);
		panelDictionary[currentPanel.name].SetActive(false);

		currentPanel = habitViewPanel;

		habitViewPanel.SetActive(true);
		HabitViewerPanel hvp = habitViewPanel.GetComponent<HabitViewerPanel>();
		hvp.SetHabit(habits[id]);

		List<string> keys = habits.Keys.ToList();

		int index = keys.IndexOf(id);
		Habit prev;
		if (index > 0 && keys.Count > 1)
		{
			index--;
			prev = habits[keys[index]];
		}
		else
		{
			//we go to the last one
			prev = habits[keys[keys.Count - 1]];
		}

		index = keys.IndexOf(id);
		Habit next;
		if (index < keys.Count - 1 && keys.Count > 1)
		{
			index++;
			next = habits[keys[index]];
		}
		else
		{
			// we go back to zero
			next = habits[keys[0]];
		}

		//clear the listeners first
		hvp.previousHabitButton.onClick.RemoveAllListeners();
		hvp.nextHabitButton.onClick.RemoveAllListeners();

		hvp.previousHabitButton.onClick.AddListener(() => OnPreviousHabitPressed(prev));
		hvp.nextHabitButton.onClick.AddListener(() => OnNextHabitPressed(next));
	}

	protected void OnNextHabitPressed(Habit next)
	{

		HabitViewerPanel hvp = habitViewPanel.GetComponent<HabitViewerPanel>();
		hvp.SetHabit(next);

		List<string> keys = habits.Keys.ToList();

		string originalKey = next.HabitID;

		int index = keys.IndexOf(originalKey);
		Habit prev;
		if (index > 0 && keys.Count > 1)
		{
			index--;
			prev = habits[keys[index]];
		}
		else
		{
			//we go to the last one
			prev = habits[keys[keys.Count-1]];
		}

		index = keys.IndexOf(originalKey);

		if (index < keys.Count - 1 && keys.Count > 1)
		{
			index++;
			next = habits[keys[index]];
		}
		else
		{
			// we go back to zero
			next = habits[keys[0]];
		}

		//clear the listeners first
		hvp.previousHabitButton.onClick.RemoveAllListeners();
		hvp.nextHabitButton.onClick.RemoveAllListeners();

		hvp.previousHabitButton.onClick.AddListener(() => OnPreviousHabitPressed(prev));
		hvp.nextHabitButton.onClick.AddListener(() => OnNextHabitPressed(next));
	}

	protected void OnPreviousHabitPressed(Habit prev)
	{
		HabitViewerPanel hvp = habitViewPanel.GetComponent<HabitViewerPanel>();
		hvp.SetHabit(prev);

		List<string> keys = habits.Keys.ToList();
		string originalKey = prev.HabitID;

		int index = keys.IndexOf(originalKey);
		if (index > 0 && keys.Count > 1)
		{
			index--;
			prev = habits[keys[index]];
		}
		else
		{
			//we go to the last one
			prev = habits[keys[keys.Count - 1]];
		}

		index = keys.IndexOf(originalKey);
		Habit next;
		if (index < keys.Count - 1 && keys.Count > 1)
		{
			index++;
			next = habits[keys[index]];
		}
		else
		{
			// we go back to zero
			next = habits[keys[0]];
		}

		//clear the listeners first
		hvp.previousHabitButton.onClick.RemoveAllListeners();
		hvp.nextHabitButton.onClick.RemoveAllListeners();

		hvp.previousHabitButton.onClick.AddListener(() => OnPreviousHabitPressed(prev));
		hvp.nextHabitButton.onClick.AddListener(() => OnNextHabitPressed(next));
	}

	protected void OnHabitRemoved(string hab)
	{
		habits.Remove(hab);
		fileStream = new FileStream(Application.persistentDataPath + "/Habits.dat", FileMode.OpenOrCreate);
		bf = new BinaryFormatter();
		bf.Serialize(fileStream, habits);
		fileStream.Close();
	}

#endregion


	

	public void RegisterCheckInHabit(TodayHabit th, string id, DateTime time)
	{
		th.yesButton.onClick.AddListener(() => CheckInHabitYESClicked(id, time));
		th.noButton.onClick.AddListener(() => CheckInHabitNOClicked(id,time));
	}

	protected void CheckInHabitNOClicked(string id, DateTime time)
	{
		if (habits[id].CompletedInstances.ContainsKey(time))
		{
			habits[id].CompletedInstances[time] = false;
		}
		else
		{
			habits[id].CompletedInstances.Add(time, false);
		}

		List<DateTime> startDates = habits[id].HabitStreaks.Keys.ToList();
		startDates.Sort();
		DateTime latestStartDate = startDates[startDates.Count - 1];


		if (habits[id].HabitStreaks[latestStartDate].End == null ) //|| habits[id].HabitStreaks[latestStartDate].End < time)
		{
			habits[id].HabitStreaks[latestStartDate].SetEnd(time);
		}

		//without this there is no serialization
		UpdateHabit(habits[id]);


	}

	protected void CheckInHabitYESClicked(string id, DateTime time)
	{
		if (habits[id].CompletedInstances.ContainsKey(time))
		{
			habits[id].CompletedInstances[time] = true;
		}
		else
		{
			habits[id].CompletedInstances.Add(time, true);
		}

		List<DateTime> startDates = habits[id].HabitStreaks.Keys.ToList();
		startDates.Sort();
		DateTime latestStartDate;

		if (startDates.Count >= 1)
		{
			latestStartDate = startDates[startDates.Count - 1];
		}
		else
		{
			latestStartDate = time;
			habits[id].HabitStreaks.Add(latestStartDate, new HabitStreak());
		}
		

		if (habits[id].HabitStreaks[latestStartDate].End == null || habits[id].HabitStreaks[latestStartDate].End < time)
		{
			habits[id].HabitStreaks[latestStartDate].SetEnd(time);
			habits[id].HabitStreaks[latestStartDate].AddInstance(time);
		}

		//without this there is no serialization
		UpdateHabit(habits[id]);

	}

	void OnViewAllButtonPressed()
	{
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewAllButton.interactable = false;
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewDayButton.interactable = true;
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewMonthButton.interactable = true;
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewTodayButton.interactable = true;
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewWeekButton.interactable = true;

		allHabitViewerPanel.SetActive(true);

		viewChoicePanel.SetActive(false);

		//previousPanels.Push(currentPanel);

		currentPanel = allHabitViewerPanel;

		allHabitViewerPanel.GetComponent<ViewAllPanel>().SpawnHabits(habits.Values.ToList());

	}


#region Weekly

	//this is really stupid!!!
	void OnViewWeekButtonPressed()
	{
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewAllButton.interactable = true;
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewDayButton.interactable = true;
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewMonthButton.interactable = true;
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewTodayButton.interactable = true;
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewWeekButton.interactable = false;

		calendarPanel.SetActive(true);
		calendarPanel.GetComponent<CalendarPanel>().weeklyPanel.SetActive(true);

		//previousPanels.Push(currentPanel);

		currentPanel = calendarPanel.GetComponent<CalendarPanel>().weeklyPanel;

		viewChoicePanel.SetActive(false);

		WeekPanel weekPanel = panelDictionary["Weekly"].GetComponent<WeekPanel>();

		weekPanel.previousWeekButton.onClick.RemoveAllListeners();
		weekPanel.nextWeekButton.onClick.RemoveAllListeners();


		int weeknum = WeekPanel.WeekNumber(DateTime.Today);
		DateTime startOfWeek = WeekPanel.FirstDateOfWeek(weeknum, DateTime.Today.Year);
		//we need to add hours and minutes to include any habits that fall on the last day
		DateTime endOfWeek = startOfWeek.AddDays(6).AddHours(23).AddMinutes(59);


		weekPanel.previousWeekButton.onClick.AddListener(delegate
		{ OnPreviousWeekButtonPressed(weeknum, DateTime.Today.Year); });
		weekPanel.nextWeekButton.onClick.AddListener(delegate
		{ OnNextWeekButtonPressed(weeknum, DateTime.Today.Year); });

		//update the text
		weekPanel.weekInfo.text = string.Format("Week of {0} ~ {1}",
			startOfWeek.ToShortDateString(), endOfWeek.ToShortDateString());


		//update the habits
		Dictionary<Habit, List<IPeriod>> thisWeeksHabits = GetHabitsBetween(startOfWeek, endOfWeek); //new Dictionary<Habit, List<IPeriod>>();

		weekPanel.SpawnHabits(thisWeeksHabits);
	}

	protected void OnNextWeekButtonPressed(int currentWeek, int currentYear)
	{
		WeekPanel weekPanel = panelDictionary["Weekly"].GetComponent<WeekPanel>();
		weekPanel.nextWeekButton.onClick.RemoveAllListeners();

		//do sanity check on week number
		int nextYear = currentWeek < 52 ? currentYear : currentYear + 1;
		int nextWeek = currentWeek < 52 ? currentWeek + 1 : 1;


		weekPanel.nextWeekButton.onClick.AddListener(delegate
		{ OnNextWeekButtonPressed(nextWeek, nextYear); });

		//remember to update the previous week button
		weekPanel.previousWeekButton.onClick.RemoveAllListeners();
		weekPanel.previousWeekButton.onClick.AddListener(delegate
		{ OnPreviousWeekButtonPressed(nextWeek, nextYear); });

		//update the date
		DateTime startOfWeek = WeekPanel.FirstDateOfWeek(nextWeek, nextYear);
		DateTime endOfWeek = startOfWeek.AddDays(6).AddHours(23).AddMinutes(59);

		//update the text
		weekPanel.weekInfo.text = string.Format("Week of {0} ~ {1}",
			startOfWeek.ToShortDateString(), endOfWeek.ToShortDateString());

		//update the habits
		Dictionary<Habit, List<IPeriod>> thisWeeksHabits = GetHabitsBetween(startOfWeek, endOfWeek); //new Dictionary<Habit, List<IPeriod>>();

		weekPanel.SpawnHabits(thisWeeksHabits);
	}

	protected void OnPreviousWeekButtonPressed(int currentWeek, int currentYear)
	{
		WeekPanel weekPanel = panelDictionary["Weekly"].GetComponent<WeekPanel>();

		weekPanel.previousWeekButton.onClick.RemoveAllListeners();
		
		//do sanity check on week number
		int previousYear = currentWeek > 1 ? currentYear : currentYear - 1;
		int previousWeek = currentWeek > 1 ? currentWeek - 1 : 52;


		weekPanel.previousWeekButton.onClick.AddListener(delegate
		{ OnPreviousWeekButtonPressed(previousWeek, previousYear); });

		//remember to update the next week button
		weekPanel.nextWeekButton.onClick.RemoveAllListeners();
		weekPanel.nextWeekButton.onClick.AddListener(delegate
		{ OnNextWeekButtonPressed(previousWeek, previousYear); });

		//update the date
		DateTime startOfWeek = WeekPanel.FirstDateOfWeek(previousWeek, previousYear);
		DateTime endOfWeek = startOfWeek.AddDays(6).AddHours(23).AddMinutes(59);

		//update the text
		weekPanel.weekInfo.text = string.Format("Week of {0} ~ {1}",
			startOfWeek.ToShortDateString(), endOfWeek.ToShortDateString());

		//update the habits
		Dictionary<Habit, List<IPeriod>> thisWeeksHabits = GetHabitsBetween(startOfWeek, endOfWeek); //new Dictionary<Habit, List<IPeriod>>();

		weekPanel.SpawnHabits(thisWeeksHabits);
	}

#endregion

#region Monthly

	//this is really stupid!!!
	void OnViewMonthButtonPressed()
	{
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewAllButton.interactable = true;
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewDayButton.interactable = true;
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewMonthButton.interactable = false;
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewTodayButton.interactable = true;
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewWeekButton.interactable = true;

		calendarPanel.SetActive(true);
		calendarPanel.GetComponent<CalendarPanel>().monthlyPanel.SetActive(true);

		MonthPanel monthPanel = panelDictionary["Monthly"].GetComponent<MonthPanel>();
		monthPanel.previousMonthButton.onClick.RemoveAllListeners();
		monthPanel.nextMonthButton.onClick.RemoveAllListeners();

		//previousPanels.Push(currentPanel);

		currentPanel = monthPanel.gameObject;

		viewChoicePanel.SetActive(false);

		monthPanel.previousMonthButton.onClick.AddListener(delegate
		{ OnPreviousMonthButtonPressed(DateTime.Today.Month, DateTime.Today.Year); });
		monthPanel.nextMonthButton.onClick.AddListener(delegate
		{ OnNextMonthButtonPressed(DateTime.Today.Month, DateTime.Today.Year); });

		monthPanel.InitializeMonth(DateTime.Today.Year,DateTime.Today.Month);

		//get the dates
		DateTime startOfMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
		DateTime endOfMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month,
			DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month)).AddHours(23).AddMinutes(59);

		//update the habits
		Dictionary<Habit, List<IPeriod>> thisMonthsHabits = GetHabitsBetween(startOfMonth,endOfMonth);

		monthPanel.SpawnHabits(thisMonthsHabits);

	}

	protected void OnNextMonthButtonPressed(int currentMonth, int currentYear)
	{
		MonthPanel monthPanel = panelDictionary["Monthly"].GetComponent<MonthPanel>();
		monthPanel.nextMonthButton.onClick.RemoveAllListeners();

		//do sanity check on month number
		int nextYear = currentMonth < 12 ? currentYear : currentYear + 1;
		int nextMonth = currentMonth < 12 ? currentMonth + 1 : 1;
		//Debug.Log(nextYear);
		//Debug.Log(nextMonth);

		monthPanel.nextMonthButton.onClick.AddListener(delegate
		{ OnNextMonthButtonPressed(nextMonth, nextYear); });

		//remember to update the previous month button
		monthPanel.previousMonthButton.onClick.RemoveAllListeners();
		monthPanel.previousMonthButton.onClick.AddListener(delegate
		{ OnPreviousMonthButtonPressed(nextMonth, nextYear); });


		//update the text
		monthPanel.InitializeMonth(nextYear, nextMonth);

		//update the dates
		DateTime startOfMonth = new DateTime(nextYear, nextMonth, 1);
		DateTime endOfMonth = new DateTime(nextYear, nextMonth,
			DateTime.DaysInMonth(nextYear, nextMonth)).AddHours(23).AddMinutes(59);

		//update the habits
		Dictionary<Habit, List<IPeriod>> thisMonthsHabits = GetHabitsBetween(startOfMonth, endOfMonth);

		monthPanel.SpawnHabits(thisMonthsHabits);
	}

	protected void OnPreviousMonthButtonPressed(int currentMonth, int currentYear)
	{
		MonthPanel monthPanel = panelDictionary["Monthly"].GetComponent<MonthPanel>();
		monthPanel.previousMonthButton.onClick.RemoveAllListeners();

		//do sanity check on month number
		int previousYear = currentMonth > 1 ? currentYear : currentYear - 1;
		int previousMonth = currentMonth > 1 ? currentMonth - 1 : 12;

		//Debug.Log(previousYear);
		//Debug.Log(previousMonth);

		monthPanel.previousMonthButton.onClick.AddListener(delegate
		{ OnPreviousMonthButtonPressed(previousMonth, previousYear); });

		//remember to update the next month button
		monthPanel.nextMonthButton.onClick.RemoveAllListeners();
		monthPanel.nextMonthButton.onClick.AddListener(delegate
		{ OnNextMonthButtonPressed(previousMonth, previousYear); });


		//update the text
		monthPanel.InitializeMonth(previousYear, previousMonth);

		//update the dates
		DateTime startOfMonth = new DateTime(previousYear, previousMonth, 1);
		DateTime endOfMonth = new DateTime(previousYear, previousMonth,
			DateTime.DaysInMonth(previousYear, previousMonth)).AddHours(23).AddMinutes(59);

		//update the habits
		Dictionary<Habit, List<IPeriod>> thisMonthsHabits = GetHabitsBetween(startOfMonth, endOfMonth);

		monthPanel.SpawnHabits(thisMonthsHabits);
	}

	protected void OnMonthDayButtonPressed(int year, int month, int day)
	{
		//we will switch to daily view
		panelDictionary["Monthly"].SetActive(false);

		panelDictionary["Daily"].SetActive(true);

		//Debug.Log(currentPanel);

		//previousPanels.Push(currentPanel);

		currentPanel = calendarPanel.GetComponent<CalendarPanel>().dailyPanel;

		//Debug.Log(currentPanel);

		//update the text
		DayPanel dayPanel = panelDictionary["Daily"].GetComponent<DayPanel>();
		DateTime underConsideration = new DateTime(year, month, day);
		dayPanel.dayInfo.text = underConsideration.DayOfWeek.ToString() + "\n (" +
			((Months)month).ToString() + " " + day + ", " + year + ")";

		dayPanel.previousDayButton.onClick.RemoveAllListeners();
		dayPanel.nextDayButton.onClick.RemoveAllListeners();

		dayPanel.previousDayButton.onClick.AddListener(delegate
		{ OnPreviousDayButtonPressed(DateTime.Today); });
		dayPanel.nextDayButton.onClick.AddListener(delegate
		{ OnNextDayButtonPressed(DateTime.Today); });

		//update the habits
		Dictionary<Habit, List<IPeriod>> thisDaysHabits = GetHabitsBetween(underConsideration, underConsideration.AddDays(1)); //new Dictionary<Habit, List<IPeriod>>();

		dayPanel.SpawnHabits(thisDaysHabits);
	}

#endregion

#region Daily

	//this is really stupid!!!
	void OnViewDayButtonPressed()
	{
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewAllButton.interactable = true;
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewDayButton.interactable = false;
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewMonthButton.interactable = true;
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewTodayButton.interactable = true;
		viewChoicePanel.GetComponent<ViewChoicePanel>().viewWeekButton.interactable = true;


		calendarPanel.SetActive(true);
		calendarPanel.GetComponent<CalendarPanel>().dailyPanel.SetActive(true);

		//previousPanels.Push(currentPanel);

		currentPanel = calendarPanel.GetComponent<CalendarPanel>().dailyPanel;

		//Debug.Log(currentPanel);

		viewChoicePanel.SetActive(false);

		//update the text
		DayPanel dayPanel = panelDictionary["Daily"].GetComponent<DayPanel>();
		dayPanel.dayInfo.text = DateTime.Today.DayOfWeek.ToString() + "\n (" +
			((Months)DateTime.Today.Month).ToString() + " " + DateTime.Today.Day + ", " + DateTime.Today.Year + ")";

		dayPanel.previousDayButton.onClick.RemoveAllListeners();
		dayPanel.nextDayButton.onClick.RemoveAllListeners();

		dayPanel.previousDayButton.onClick.AddListener(delegate
		{ OnPreviousDayButtonPressed(DateTime.Today); });
		dayPanel.nextDayButton.onClick.AddListener(delegate
		{ OnNextDayButtonPressed(DateTime.Today); });

		//update the habits
		Dictionary<Habit, List<IPeriod>> thisDaysHabits = GetHabitsBetween(DateTime.Today, DateTime.Today.AddDays(1)); //new Dictionary<Habit, List<IPeriod>>();

		dayPanel.SpawnHabits(thisDaysHabits);
	}

	protected void OnNextDayButtonPressed(DateTime day)
	{
		DayPanel dayPanel = panelDictionary["Daily"].GetComponent<DayPanel>();
		dayPanel.nextDayButton.onClick.RemoveAllListeners();

		DateTime nextDay = day.AddDays(1);


		dayPanel.nextDayButton.onClick.AddListener(delegate
		{ OnNextDayButtonPressed(nextDay); });

		////remember to update the previous day button
		dayPanel.previousDayButton.onClick.RemoveAllListeners();
		dayPanel.previousDayButton.onClick.AddListener(delegate
		{ OnPreviousDayButtonPressed(nextDay); });


		////update the text
		dayPanel.dayInfo.text = nextDay.DayOfWeek.ToString() + "\n (" +
			((Months)nextDay.Month).ToString() + " " + nextDay.Day + ", " + nextDay.Year + ")";

		//update the habits
		Dictionary<Habit, List<IPeriod>> thisDaysHabits = GetHabitsBetween(nextDay, nextDay.AddDays(1)); //new Dictionary<Habit, List<IPeriod>>();

		dayPanel.SpawnHabits(thisDaysHabits);
	}

	protected void OnPreviousDayButtonPressed(DateTime day)
	{
		DayPanel dayPanel = panelDictionary["Daily"].GetComponent<DayPanel>();
		dayPanel.previousDayButton.onClick.RemoveAllListeners();

		DateTime previousDay = day.AddDays(-1);

		dayPanel.previousDayButton.onClick.AddListener(delegate
		{ OnPreviousDayButtonPressed(previousDay); });

		//remember to update the next day button
		dayPanel.nextDayButton.onClick.RemoveAllListeners();
		dayPanel.nextDayButton.onClick.AddListener(delegate
		{ OnNextDayButtonPressed(previousDay); });


		//update the text
		dayPanel.dayInfo.text = previousDay.DayOfWeek.ToString() + "\n (" +
			((Months)previousDay.Month).ToString() + " " + previousDay.Day + ", " + previousDay.Year + ")";

		//update the habits
		Dictionary<Habit, List<IPeriod>> thisDaysHabits = GetHabitsBetween(previousDay, previousDay.AddDays(1)); //new Dictionary<Habit, List<IPeriod>>();

		dayPanel.SpawnHabits(thisDaysHabits);
	}

#endregion

	Dictionary<Habit, List<IPeriod>> GetHabitsBetween(DateTime start, DateTime end)
	{
		Dictionary<Habit, List<IPeriod>> habitsBetween = new Dictionary<Habit, List<IPeriod>>();

		//Debug.Log(habits.Count);
		foreach (var habit in habits)
		{
			//Debug.Log(habit.Value.HabitName);
			//RecurrencePatternSerializer serializer = new RecurrencePatternSerializer();
			//var newPattern = serializer.Deserialize(fileStream, System.Text.Encoding.UTF8);
			//RecurrencePatternEvaluator re = new RecurrencePatternEvaluator(newPattern);
			start = start.AddSeconds(1);
			end = end.AddSeconds(1);
			for (int i = 0; i < habit.Value.RepititionRules.Count; i++)
			{
				var pa = new RecurrencePattern(habit.Value.RepititionRules[i]);
				//Debug.Log(pa.ToString());
				RecurrencePatternEvaluator re = new RecurrencePatternEvaluator(pa);
				//HashSet<IPeriod> periods = re.Evaluate(new CalDateTime(DateTime.Now),
				//	habit.Value.Start.AddSeconds(1), end.AddSeconds(1), false);
				HashSet<IPeriod> periods = re.Evaluate(new CalDateTime(habit.Value.Start.AddSeconds(1)), start,
					end, false);
				//Debug.Log(periods.Count);
				//Debug.Log(habit.Value.Start);
				//Debug.Log(start);
				//Debug.Log(end);
				foreach (var period in periods)
				{
					//Debug.Log(period);
					if (period.StartTime.Value > start)
					{
						if (!habit.Value.CompletedInstances.ContainsKey(period.StartTime.Value))
						{
							//Debug.Log(period.StartTime.Value);
							//Debug.Log(thisWeeksHabits.ContainsKey(period));
							if (habitsBetween.ContainsKey(habit.Value))
							{
								habitsBetween[habit.Value].Add(period);
							}
							else
							{
								habitsBetween.Add(habit.Value, new List<IPeriod>() { period });
							}

						}
					}

					//period.StartTime.
				}
			}

		}

		return habitsBetween;
	}

#region Swiping
	[SerializeField]
	float deadZone = 50f;
	[SerializeField]
	float doubleTapDelta = 0.5f;

	bool tap, doubleTap, swipeLeft, swipeRight, swipeUp, swipeDown;
	Vector2 swipeDelta, startTouch;
	float lastTap;
	float sqrDeadzone;

	void DetectSwipesStandalone()
	{
		if (Input.GetMouseButtonDown(0))
		{
		
				tap = true;
				startTouch = Input.mousePosition;
				doubleTap = Time.time - lastTap < doubleTapDelta;
				//Debug.Log(Time.time - lastTap);
				lastTap = Time.time;
		}
		else if (Input.GetMouseButtonUp(0))
		{
			startTouch = swipeDelta = Vector2.zero;
		}
		swipeDelta = Vector2.zero;

		if (startTouch != Vector2.zero && Input.GetMouseButton(0))
		{
			swipeDelta = new Vector2(Input.mousePosition.x, Input.mousePosition.y) - startTouch;
		}

		if (swipeDelta.sqrMagnitude > sqrDeadzone)
		{
			float x = swipeDelta.x;
			float y = swipeDelta.y;
			if (Mathf.Abs(x) > Mathf.Abs(y))
			{
				if (x < 0)
				{
					swipeLeft = true;
				}
				else
				{
					swipeRight = true;
				}
			}
			else
			{
				if (y < 0)
				{
					swipeDown = true;
				}
				else
				{
					swipeUp = true;
				}
			}
			//we detected a large enough swipe, so now we will reset
			startTouch = swipeDelta = Vector2.zero;
		}

	}

	void DetectSwipesMobile()
	{
		if (Input.touches.Length != 0)
		{
			if (Input.touches[0].phase == TouchPhase.Began)
			{
				tap = true;
				startTouch = Input.touches[0].position;
				doubleTap = Time.time - lastTap < doubleTapDelta;
				//Debug.Log(Time.time - lastTap);
				lastTap = Time.time;
			}
			else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
			{
				startTouch = swipeDelta = Vector2.zero;
			}
		}
		swipeDelta = Vector2.zero;

		if (startTouch != Vector2.zero && Input.touches.Length != 0)
		{
			swipeDelta = Input.touches[0].position - startTouch;
		}

		if (swipeDelta.sqrMagnitude > sqrDeadzone)
		{
			float x = swipeDelta.x;
			float y = swipeDelta.y;
			if (Mathf.Abs(x) > Mathf.Abs(y))
			{
				if (x < 0)
				{
					swipeLeft = true;
				}
				else
				{
					swipeRight = true;
				}
			}
			else
			{
				if (y < 0)
				{
					swipeDown = true;
				}
				else
				{
					swipeUp = true;
				}
			}
			//we detected a large enough swipe, so now we will reset
			startTouch = swipeDelta = Vector2.zero;
		}

	}

#endregion
}
