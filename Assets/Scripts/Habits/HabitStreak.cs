﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class HabitStreak
{
	DateTime start;
	DateTime end;

	HashSet<DateTime> completedInstances = new HashSet<DateTime>();

	public HabitStreak()
	{

	}

	public HabitStreak(DateTime s)
	{
		start = s;
	}

	public DateTime Start
	{
		get { return start; }
	}

	public DateTime End
	{
		get { return end; }
	}

	public HashSet<DateTime> Instances
	{
		get { return completedInstances; }
	}

	public void SetStart(DateTime s)
	{
		start = s;
	}

	public void SetEnd(DateTime e)
	{
		end = e;
		//AddInstance(e);
	}

	public void AddInstance(DateTime i)
	{
		if (!completedInstances.Contains(i))
		{
			completedInstances.Add(i);
		}
	}
}
