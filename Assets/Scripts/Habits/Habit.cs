﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ical.Net.DataTypes;

[Serializable]
public class Habit: IEqualityComparer<Habit>
{
	string habitID;
	string habitName;
	//repititionclass
	List<string> repititionRules = new List<string>();
	DateTime start;
	float habitColorR;
	float habitColorG;
	float habitColorB;

	int notificationID;

	RepUnit repUnit;

	Dictionary<DateTime, bool> completedinstances = new Dictionary<DateTime, bool>();

	Dictionary<DateTime, HabitStreak> habitStreaks = new Dictionary<DateTime,HabitStreak>();

	public Habit()
	{
		habitID = Guid.NewGuid().ToString();
		//habitColorR = UnityEngine.Random.Range(0f, 1f);
		//habitColorG = UnityEngine.Random.Range(0f, 1f);
		//habitColorB = UnityEngine.Random.Range(0f, 1f);
	}

	public Habit(string n, List<string> r)
	{
		habitID = Guid.NewGuid().ToString();
		//habitColorR = UnityEngine.Random.Range(0f, 1f);
		//habitColorG = UnityEngine.Random.Range(0f, 1f);
		//habitColorB = UnityEngine.Random.Range(0f, 1f);
		habitName = n;
		repititionRules = r;
	}

	public Habit(string n, List<string> r, DateTime s, RepUnit ru)
	{
		habitID = Guid.NewGuid().ToString();
		habitColorR = UnityEngine.Random.Range(0f, 1f);
		habitColorG = UnityEngine.Random.Range(0f, 1f);
		habitColorB = UnityEngine.Random.Range(0f, 1f);
		habitName = n;
		repititionRules = r;
		start = s;
		repUnit = ru;
	}

	public Habit(string n, List<string> r, DateTime s, RepUnit ru, Color c)
	{
		habitID = Guid.NewGuid().ToString();
		habitColorR = c.r;
		habitColorG = c.g;
		habitColorB = c.b;
		habitName = n;
		repititionRules = r;
		start = s;
		repUnit = ru;
	}

	public float SuccessPercentage
	{
		get
		{
			int success = 0;
			int total = completedinstances.Count == 0? 1 : completedinstances.Count;
			foreach (var instance in completedinstances)
			{
				if (instance.Value)
				{
					success++;
				}
			}
			return (float)success/total * 100f;
		}

	}

	public float SuccessPercentAtDate(DateTime until)
	{
		int success = 0;
		int total = 0;
		if (completedinstances.Count == 0)
		{
			return 0f;
		}

		foreach (var instance in completedinstances)
		{
			if (instance.Key <= until)
			{
				if (instance.Value)
				{
					//Debug.Log("increasing success");
					success++;
				}
				total++;
			}
			else
			{
				break;
			}
				
		}
		return (float)success / total * 100f;
	}

	public void SetName(string n)
	{
		habitName = n;
	}

	public int LongestStreak
	{
		get
		{
			int max = 0;
			foreach (var item in habitStreaks)
			{
				if (item.Value.Instances.Count > max)
				{
					max = item.Value.Instances.Count;
				}
				
			}
			return max;
		}
	}

	public string HabitName
	{
		get { return habitName; }
	}

	public string HabitID
	{
		get { return habitID; }
	}

	public Color HabitColor
	{
		get { return new Color(habitColorR,habitColorG,habitColorB); }
	}

	public float HabitColorR
	{
		get { return habitColorR; }
	}

	public float HabitColorG
	{
		get { return habitColorG; }
	}

	public float HabitColorB
	{
		get { return habitColorB; }
	}

	public void SetColor(float r, float g, float b)
	{
		habitColorR = r;
		habitColorG = g;
		habitColorB = b;
	}

	public void SetColor(Color color)
	{
		habitColorR = color.r;
		habitColorG = color.g;
		habitColorB = color.b;
	}

	public void SetID(int id)
	{
		notificationID = id;
	}


	public List<string> RepititionRules
	{
		get { return repititionRules; }
	}

	public void SetRepititionRules(List<string> r)
	{
		repititionRules = r;
	}

	public DateTime Start
	{
		get { return start; }
	}

	public Dictionary<DateTime,bool> CompletedInstances
	{
		get { return completedinstances; }
	}

	public Dictionary<DateTime,HabitStreak> HabitStreaks
	{
		get { return habitStreaks; }
		set { habitStreaks = value; }
	}

	public bool Equals(Habit x, Habit y)
	{
		return x.HabitID.Equals(y.HabitID);
	}

	public int GetHashCode(Habit obj)
	{
		return obj.HabitID.GetHashCode();
	}
}

[Serializable]
public class RepUnit
{
	public int numberOfReps;
	public RepititionUnitType unitType;
}
