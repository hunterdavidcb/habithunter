﻿

public enum Ordinal
{
	ThirdToLast = -3,
	SecondToLast = -2,
	Last = -1,
	First = 1,
	Second = 2,
	Third = 3,
	Fourth = 4,
	Fifth = 5	
}

public enum Months
{
	January = 1,
	February = 2,
	March = 3,
	April = 4,
	May = 5,
	June = 6,
	July = 7,
	August = 8,
	September = 9,
	October = 10,
	November = 11,
	December = 12
}

public enum RepititionType
{
	SECONDLY,
	MINUTELY,
	HOURLY,
	DAILY,
	WEEKLY,
	MONTHLY,
	YEARLY
}

public enum PositionTypes
{
	None,
	ByMonth,
	ByMonthDay,
	ByDay,
	BySetPos,
}

public enum EndType
{
	NEVER,
	AFTER,
	ON
}

public enum RepititionUnitType
{
	Times,
	Minutes,
	Hours
}


