using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Ical.Net.Evaluation;
using Ical.Net.Interfaces.DataTypes;
using Ical.Net.Interfaces.General;
using Ical.Net.Serialization.iCalendar.Serializers.DataTypes;
using Ical.Net.Utility;

namespace Ical.Net.DataTypes
{
    /// <summary>
    /// An iCalendar representation of the <c>RRULE</c> property.
    /// </summary>
    public class RecurrencePattern : EncodableDataType, IRecurrencePattern
    {
        private int _interval = int.MinValue;
        private RecurrenceRestrictionType? _restrictionType;
        private RecurrenceEvaluationModeType? _evaluationMode;

        public FrequencyType Frequency { get; set; }

        public DateTime Until { get; set; } = DateTime.MinValue;

        public int Count { get; set; } = int.MinValue;

        public int Interval
        {
            get
            {
                return _interval == int.MinValue
                    ? 1
                    : _interval;
            }
            set { _interval = value; }
        }

        public List<int> BySecond { get; set; } = new List<int>(16);

        public List<int> ByMinute { get; set; } = new List<int>(16);

        public List<int> ByHour { get; set; } = new List<int>(16);

        public List<IWeekDay> ByDay { get; set; } = new List<IWeekDay>(16);

        public List<int> ByMonthDay { get; set; } = new List<int>(16);

        public List<int> ByYearDay { get; set; } = new List<int>(16);

        public List<int> ByWeekNo { get; set; } = new List<int>(16);

        public List<int> ByMonth { get; set; } = new List<int>(16);

        public List<int> BySetPosition { get; set; } = new List<int>(16);

        public DayOfWeek FirstDayOfWeek { get; set; } = DayOfWeek.Monday;

        public RecurrenceRestrictionType RestrictionType
        {
            get
            {
                // NOTE: Fixes bug #1924358 - Cannot evaluate Secondly patterns
                if (_restrictionType != null)
                {
                    return _restrictionType.Value;
                }
                return Calendar?.RecurrenceRestriction ?? RecurrenceRestrictionType.Default;
            }
            set { _restrictionType = value; }
        }

        public RecurrenceEvaluationModeType EvaluationMode
        {
            get
            {
                // NOTE: Fixes bug #1924358 - Cannot evaluate Secondly patterns
                if (_evaluationMode != null)
                {
                    return _evaluationMode.Value;
                }
                return Calendar?.RecurrenceEvaluationMode ?? RecurrenceEvaluationModeType.Default;
            }
            set { _evaluationMode = value; }
        }

        public RecurrencePattern()
        {
            SetService(new RecurrencePatternEvaluator(this));
        }

        public RecurrencePattern(FrequencyType frequency) : this(frequency, 1) {}

        public RecurrencePattern(FrequencyType frequency, int interval) : this()
        {
            Frequency = frequency;
            Interval = interval;
        }

        public RecurrencePattern(string value) : this()
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return;
            }
            var serializer = new RecurrencePatternSerializer();
            CopyFrom(serializer.Deserialize(new StringReader(value)) as ICopyable);
        }

        public override string ToString()
        {
            var serializer = new RecurrencePatternSerializer();
            return serializer.SerializeToString(this);
        }

		//added by David Hunter
		public string ConvertToSentence()
		{
			string result = "Every " + (Interval == 1? "": Interval.ToString());

			switch (Frequency)
			{
				case FrequencyType.None:
					break;
				case FrequencyType.Secondly:
					result += Interval == 1 ? "second" : "seconds";
					break;
				case FrequencyType.Minutely:
					result += Interval == 1 ? " minute" : " minutes";
					break;
				case FrequencyType.Hourly:
					result += Interval == 1 ? " hour" : " hours";
					break;
				case FrequencyType.Daily:
					result += Interval == 1 ? " day" : " days";
					result += " at " + ByHour[0].ToString("D2") + ":" + ByMinute[0].ToString("D2");
					break;
				case FrequencyType.Weekly:
					result += Interval == 1 ? " week" : " weeks";
					//add info for days of week
					result += ", on ";
					for (int i = 0; i < ByDay.Count; i++)
					{
						result += ByDay[i].DayOfWeek.ToString();
						if (i != ByDay.Count-1 && i != ByDay.Count - 2)
						{
							result += ", ";
						}
						else if(i == ByDay.Count - 2)
						{
							result += ", and ";
						}
					}
					result += " at " + ByHour[0].ToString("D2") + ":" + ByMinute[0].ToString("D2");
					break;
				case FrequencyType.Monthly:
					result += Interval == 1 ? "month" : "months";
					result += ", on the ";
					//we are setting the recurrence using the day of the month, 
					// ie, 1st, 10th, 23rd
					if (ByMonthDay.Count != 0)
					{
						for (int i = 0; i < ByMonthDay.Count; i++)
						{
							result += AddOrdinal(ByMonthDay[i]);
							if (i != ByMonthDay.Count - 1)
							{
								result += ", ";
							}
						}
						
					}
					else
					{
						for (int i = 0; i < BySetPosition.Count; i++)
						{
							//we want to add spaces in between the words
							string t = ((Ordinal)BySetPosition[i]).ToString();
							for (int p = 1; p < t.Length; p++)
							{
								int index = t.IndexOfAny(new char[] {'A','B','C','D','E','F','G','H','I','J','K','L','M','N'
									,'O','P','Q','R','S','T','U','V','W','X','Y','Z'}, p);
								if (index > -1)
								{
									t = t.Insert(index - 1, " ");
								}
							}
							result +=t;
							if (i != BySetPosition.Count - 1 && BySetPosition.Count > 2)
							{
								result += ", ";
							}
							else if (i != BySetPosition.Count - 1 && BySetPosition.Count == 2)
							{
								result += "and ";
							}
						}

						for (int i = 0; i < ByDay.Count; i++)
						{
							result += ByDay[i].DayOfWeek.ToString();
							if (i != ByDay.Count - 1 && i != ByDay.Count - 2)
							{
								result += ", ";
							}
							else if (i == ByDay.Count - 2)
							{
								result += ", and ";
							}
						}
					}

					result += " at " + ByHour[0].ToString("D2") + ":" + ByMinute[0].ToString("D2");
					break;
				case FrequencyType.Yearly:
					result += Interval == 1 ? "year" : "years";

					result += ", on the ";
					//we are setting the recurrence using the day of the month, 
					// ie, 1st, 10th, 23rd
					if (ByMonthDay.Count != 0)
					{
						for (int i = 0; i < ByMonthDay.Count; i++)
						{
							result += AddOrdinal(ByMonthDay[i]);
							if (i != ByMonthDay.Count - 1 && i != ByMonthDay.Count-2)
							{
								result += ", ";
							}
							else if (i == ByMonthDay.Count - 2)
							{
								result += ", and ";
							}
						}

						

						if (ByMonth.Count != 0)
						{
							result += " of ";
							for (int i = 0; i < ByMonth.Count; i++)
							{
								result += ((Months)ByMonth[i]).ToString();
								if (i != ByMonth.Count - 1 && i != ByMonth.Count - 2)
								{
									result += ", ";
								}
								else if (i == ByMonth.Count - 2)
								{
									result += ", and ";
								}
							}
						}
					}
					else
					{
						for (int i = 0; i < BySetPosition.Count; i++)
						{
							//we want to add spaces in between the words
							string t = ((Ordinal)BySetPosition[i]).ToString();
							for (int p = 1; p < t.Length; p++)
							{
								int index = t.IndexOfAny(new char[] {'A','B','C','D','E','F','G','H','I','J','K','L','M','N'
									,'O','P','Q','R','S','T','U','V','W','X','Y','Z'}, p);
								if (index > -1)
								{
									t = t.Insert(index - 1, " ");
								}
							}
							result += t;
							if (i != BySetPosition.Count - 1 && BySetPosition.Count > 2)
							{
								result += ", ";
							}
							else if (i != BySetPosition.Count - 1 && BySetPosition.Count == 2)
							{
								result += "and ";
							}
						}

						for (int i = 0; i < ByDay.Count; i++)
						{
							result += ByDay[i].DayOfWeek.ToString();
							if (i != ByDay.Count - 1 && i != ByDay.Count - 2)
							{
								result += ", ";
							}
							else if (i == ByDay.Count - 2)
							{
								result += ", and ";
							}
						}

						if (ByMonth.Count != 0)
						{
							result += " of ";
							for (int i = 0; i < ByMonth.Count; i++)
							{
								result += ((Months)ByMonth[i]).ToString();
								if (i != ByMonth.Count - 1 && i != ByMonth.Count - 2)
								{
									result += ", ";
								}
								else if (i == ByMonth.Count - 2)
								{
									result += ", and ";
								}
							}
						}
					}

					result += " at " + ByHour[0].ToString("D2") + ":" + ByMinute[0].ToString("D2");
					break;
				default:
					break;
			}

			return result;
		}

		public static string AddOrdinal(int num)
		{
			if (num <= 0) return num.ToString();

			switch (num % 100)
			{
				case 11:
				case 12:
				case 13:
					return num + "th";
			}

			switch (num % 10)
			{
				case 1:
					return num + "st";
				case 2:
					return num + "nd";
				case 3:
					return num + "rd";
				default:
					return num + "th";
			}

		}

		protected bool Equals(RecurrencePattern other)
        {
            return (Interval == other.Interval)
                && (RestrictionType == other.RestrictionType)
                && (EvaluationMode == other.EvaluationMode)
                && (Frequency == other.Frequency)
                && Until.Equals(other.Until)
                && (Count == other.Count)
                && (FirstDayOfWeek == other.FirstDayOfWeek)
                && CollectionEquals(BySecond, other.BySecond)
                && CollectionEquals(ByMinute, other.ByMinute)
                && CollectionEquals(ByHour, other.ByHour)
                && CollectionEquals(ByDay, other.ByDay)
                && CollectionEquals(ByMonthDay, other.ByMonthDay)
                && CollectionEquals(ByYearDay, other.ByYearDay)
                && CollectionEquals(ByWeekNo, other.ByWeekNo)
                && CollectionEquals(ByMonth, other.ByMonth)
                && CollectionEquals(BySetPosition, other.BySetPosition);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((RecurrencePattern) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Interval.GetHashCode();
                hashCode = (hashCode * 397) ^ RestrictionType.GetHashCode();
                hashCode = (hashCode * 397) ^ EvaluationMode.GetHashCode();
                hashCode = (hashCode * 397) ^ (int) Frequency;
                hashCode = (hashCode * 397) ^ Until.GetHashCode();
                hashCode = (hashCode * 397) ^ Count;
                hashCode = (hashCode * 397) ^ (int)FirstDayOfWeek;
                hashCode = (hashCode * 397) ^ CollectionHelpers.GetHashCode(BySecond);
                hashCode = (hashCode * 397) ^ CollectionHelpers.GetHashCode(ByMinute);
                hashCode = (hashCode * 397) ^ CollectionHelpers.GetHashCode(ByHour);
                hashCode = (hashCode * 397) ^ CollectionHelpers.GetHashCode(ByDay);
                hashCode = (hashCode * 397) ^ CollectionHelpers.GetHashCode(ByMonthDay);
                hashCode = (hashCode * 397) ^ CollectionHelpers.GetHashCode(ByYearDay);
                hashCode = (hashCode * 397) ^ CollectionHelpers.GetHashCode(ByWeekNo);
                hashCode = (hashCode * 397) ^ CollectionHelpers.GetHashCode(ByMonth);
                hashCode = (hashCode * 397) ^ CollectionHelpers.GetHashCode(BySetPosition);
                return hashCode;
            }
        }

        public override void CopyFrom(ICopyable obj)
        {
            base.CopyFrom(obj);
            if (!(obj is IRecurrencePattern))
            {
                return;
            }

            var r = (IRecurrencePattern) obj;

            Frequency = r.Frequency;
            Until = r.Until;
            Count = r.Count;
            Interval = r.Interval;
            BySecond = new List<int>(r.BySecond);
            ByMinute = new List<int>(r.ByMinute);
            ByHour = new List<int>(r.ByHour);
            ByDay = new List<IWeekDay>(r.ByDay);
            ByMonthDay = new List<int>(r.ByMonthDay);
            ByYearDay = new List<int>(r.ByYearDay);
            ByWeekNo = new List<int>(r.ByWeekNo);
            ByMonth = new List<int>(r.ByMonth);
            BySetPosition = new List<int>(r.BySetPosition);
            FirstDayOfWeek = r.FirstDayOfWeek;
            RestrictionType = r.RestrictionType;
            EvaluationMode = r.EvaluationMode;
        }

        private static bool CollectionEquals<T>(IEnumerable<T> c1, IEnumerable<T> c2) => c1.SequenceEqual(c2);
    }
}